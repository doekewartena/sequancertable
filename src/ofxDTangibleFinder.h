//
//  ofxTangibleFinder.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/24/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
//#include "ofxDTangible.h"
#include "ofxDTangibleBase.h"
#include "ofxDKnobAngleDetector.h"
#include "ofxDDetectColor.h"



class ofxDTangibleFinder {
    
private:
    int _minDiameter;
    int _maxDiameter;
    
    int _maxTangibles;
    
    float fMaxAspectDeviation = 0.5f;
    
    // if the centroid is more away then the actual centroid of the boundingrect
    // then no tangible will be formed
    float fMaxCentroidDeviation = 0.5f;
    
    //
    bool setupMinDiameter, setupMaxDiameter, setupMaxTangibles  = false;
    bool _setupCorrect = false;
    
    void printNotSetupError(string what);
    
    vector<shared_ptr<ofxDTangibleBase> > _tangibles;
    
    
    // here we save contourFinders in steps of 32
    // this is to avoid allocating images
    // for different sizes, which is slow
    vector<ofxCvContourFinder*> contourFindersBy32;
    
    
    ofxCvGrayscaleImage* getCvGrayscaleImageBy32(int origonalWidth, int originalHeight);
    vector<ofxCvGrayscaleImage*> unusedCvGrayscaleImagesBy32;    
    
    ofxCvColorImage* getCvCvColorImageBy32(int origonalWidth, int originalHeight);
    vector<ofxCvColorImage*> unusedCvColorImagesBy32;
    
    
    ofxCvContourFinder* getContourFinderBy32(const ofxCvGrayscaleImage* forImage);
    
    bool _storeOriginalBlobAreas = false;
    bool _storeThresholdAreas = false;
    
public:
    
    // in this vector we store images we use
    // to prepare blobs for a 2nd detection
    // on color to decide there valid for a tangible
    // (name is maybe a bit misleading)
    vector<ofxCvGrayscaleImage*> colorDetectionImagesBy32;
    
    // for monitor purpose
    // be aware that they are ready the frame after the first call
    void storeOriginalBlobAreas(bool b) { _storeOriginalBlobAreas = b; }
    void storeThresholdAreas(bool b) { _storeThresholdAreas = b; }
    vector<ofxCvColorImage*> originalBlobAreasBy32;
    vector<ofxCvGrayscaleImage*> thresholdAreasBy32;
    
        
    // TODO
    // deconstructor
    
    void draw();
    // can those be inline methods?
    void setMinDiameter(int d) { _minDiameter = d; setupMinDiameter = true;}
    void setMaxDiameter(int d) { _maxDiameter = d; setupMaxDiameter = true;}
    
    int getMinDiameter() const { return _minDiameter; }
    int getMaxDiameter() const { return _maxDiameter; }
    
    int getMaxTangibles() const { return _maxTangibles; }
    void setMaxTangibles(int m) { _maxTangibles = m; setupMaxTangibles = true; }
    
    // 0 is no tolerance in deviation
    float getMaxAspectDeviation() const { return fMaxAspectDeviation; }
    void setMaxAspectDeviation(float v) { fMaxAspectDeviation = v; }
    
    float getMaxCentroidDeviation() const { return fMaxCentroidDeviation; }
    void setMaxCentroidDeviation(float v) { fMaxCentroidDeviation = v; }
    
    
    int findTangibles(ofxCvGrayscaleImage& input, ofxCvColorImage &img, const vector<ofxDDetectColor*> &detectColors);
    
    
    bool blobHasValidAspectRatio(ofxCvBlob &blob);
    
    const vector<shared_ptr<ofxDTangibleBase> > &getTangibles() const { return _tangibles;}

    
    
    bool checkForSetupCorrect ();
    
    
    ofxCvContourFinder 	contourFinder;
    
};
