//
//  ImageFeader.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/24/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"

// Base class
class TangibleFeeder
{
public:
    
    int _feedWidth;
    int _feedHeight;
    
    ofxCvColorImage			colorImg;
    
    ofxCvGrayscaleImage 	grayImage;
    ofxCvGrayscaleImage 	grayBg;
    ofxCvGrayscaleImage 	grayDiff;
    
    // public methods
    
    void init();
    
    virtual bool hasUpdate() = 0;
    virtual void updateColorImage() = 0;
    
    ofxCvGrayscaleImage& getTangibleImage();
    
    int feedWidth() const { return _feedWidth ; }
    int feedHeight() const { return _feedHeight ; }
    
    void setNewBackground();
    
    int getThreshold() const { return iThreshold;}
    void setThreshold(int v);
    void incrementThreshold();
    void decrementThreshold();
    
private:
    int iThreshold = 80;
    bool bSetNewBackground = true;
    
    int getTangibleImageFrameNumCall = -1;
    
};