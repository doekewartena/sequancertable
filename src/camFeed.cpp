//
//  camFeed.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/24/13.
//
//

#include "camFeed.h"



CamFeed::CamFeed(int deviceId, int width, int height, int frameRate) {
    
    vector<ofVideoDevice> devices = vidGrabber.listDevices();
    
    // output available cams
    printf("devices.size(): %lu\n", devices.size());
    
    for(int i = 0; i < devices.size(); i++){
        cout << devices[i].id << ": " << devices[i].deviceName;
        if( devices[i].bAvailable ){
            cout << endl;
        }else{
            cout << " - unavailable " << endl;
        }
    }
    
    vidGrabber.setDeviceID(deviceId); // 1
    vidGrabber.setDesiredFrameRate(frameRate); // 30, 15, 1
    vidGrabber.initGrabber(width, height);
    
    _feedWidth = vidGrabber.getWidth();
    _feedHeight = vidGrabber.getHeight();
    
    init();
}


// . . . . . . . . . . . . . . . . . . . . . . . . .




bool CamFeed::hasUpdate() {
    vidGrabber.update();
    return vidGrabber.isFrameNew();
}




// . . . . . . . . . . . . . . . . . . . . . . . . .


void CamFeed::updateColorImage() {
    colorImg.setFromPixels(vidGrabber.getPixels(), feedWidth(), feedHeight());
    // we call setNewBackground since the webcam starts a bit slow
    if(ofGetFrameNum() == 60) {
        setNewBackground();
    }
}



// . . . . . . . . . . . . . . . . . . . . . . . . .