//
//  ofxDTangibleBase.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/29/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxDDetectColor.h"


// we use this struct to store info about the edges
// of the knob angle indicator
struct KnobPtsSideInfo {
    
    int startIndex = -1;
    // exclusive
    int endIndex = -1;
    
    int length() { return endIndex - startIndex; }
    
    
};

class ofxDTangibleBase : public ofxCvBlob {
    
    private:
        //
    
    
        bool _angleDetected = false;
    
        float _angle;
        // when the angle changes we set it as a target
        // this way we can interpolate if required
        // maybe implement in a custom tangible
        //float _targetAngle;
    
        // this is the actual color
        ofColor _color;
        // we store the frame num when we set the color
        int _colorDetectFrameNum = -1;
        //bool _colorIsSet = false;

    
    
    public:
        // we store the detectColor
        // this way we can make easier changes to the color
        // or the fuzziness to adjust the detection result
        ofxDDetectColor *detectColor;
    

    
    
        // used in globalTangibleList
        ofxDTangibleBase *previous = NULL;
        ofxDTangibleBase *next = NULL;
        
        bool inGlobalList = false;
    
        // we store the threshold image
        // since it's color and fuzziness based
        // this way we have a clue when we adjust it
        ofxCvGrayscaleImage *thresholdImage = NULL;
        // to draw the threshold image at the correct position
        // we have to offset it
        ofPoint thresholdImageOffset;
    
        const char *className;
    
        // those will hold an startIndex and an endIndex to form 2
        // lines that are used to decide the angle if knobAngleDetector is used
        KnobPtsSideInfo ptsAngleInfoFirst;
        KnobPtsSideInfo ptsAngleInfoSecond;
    
        // the first angle that was detected
        float dropAngle;
    
        // TODO
        // the amount of times the tangible made a full turn
        // this will decrease when rotating ccw
        float nOfTurns;
    
        ofxDTangibleBase(const ofxCvBlob& base, const char *className, ofxDDetectColor *detectColor, const ofxCvGrayscaleImage *thresholdImage) : ofxCvBlob(base) {
            this->className = className;
            this->detectColor = detectColor;
                        
            this->thresholdImage = new ofxCvGrayscaleImage(*thresholdImage);
            
            this->thresholdImage->setROI(0, 0, base.boundingRect.width, base.boundingRect.height);
            
            setBlobValues(base);
            
            
        }
    
        ofxDTangibleBase(const ofxDTangibleBase& base, const char *className) : ofxCvBlob(base) {
            this->className = className;
            
            //this->setColor(base.getColor());
            this->setAngle(base.getAngle());
            //this->setTargetAngle(base.getTargetAngle());
            this->detectColor = base.detectColor;
            
            //this->thresholdImage = new ofxCvGrayscaleImage(*base.thresholdImage);
            
            // for some reason we get an error when trying to create
            // a copy while ROI is set
            // we bypass this by restting the ROI
            ofxCvGrayscaleImage& mom = const_cast<ofxCvGrayscaleImage&>(*base.thresholdImage);
            mom.resetROI();
            this->thresholdImage = new ofxCvGrayscaleImage(mom);
            this->thresholdImage->setROI(0, 0, base.boundingRect.width, base.boundingRect.height);
            // restore in case needed
            mom.setROI(0, 0, base.boundingRect.width, base.boundingRect.height);
            
            this->thresholdImageOffset = base.thresholdImageOffset;
            
            //this->thresholdImage->setROI(thresholdImageOffset.x, thresholdImageOffset.y, boundingRect.width, boundingRect.height);
            
            setBlobValues(base);
        }
    
        ~ofxDTangibleBase() {
            // previous and next should stay untouched
            delete thresholdImage;
        }
    
//        bool operator < (const ofxDTangibleBase& b) const
//        {
//            return (area < b.area);
//        }

        
        //virtual void operator = ( const ofxCvBlob& mom);
        //bool operator==(const ofxDTangibleBase &other) const;
    
        void setBlobValues(const ofxCvBlob& blob) {
            this->area = blob.area;
            this->length = blob.length;
            this->boundingRect = blob.boundingRect;
            this->centroid = blob.centroid;
            this->hole = blob.hole;
            this->pts = blob.pts;
            this->nPts = blob.nPts;
        }
    
    
        void setColor(ofColor c) { _color = c; _colorDetectFrameNum = ofGetFrameNum(); }
        ofColor getColor() const { return _color;}
        int colorDetectFrameNum() const { return _colorDetectFrameNum; }

        //bool colorIsSet() const { return _colorIsSet; }
    
    
        //void setAngle(float angle) { _angle = angle; if(!_isKnob) { _targetAngle = angle; _isKnob = true;} }
    void setAngle(float angle) { _angle = angle; if (_angleDetected == false) dropAngle = angle; _angleDetected = true;}
        float getAngle() const { return _angle; }
        bool angleDetected() const { return _angleDetected ;}
        //void setTargetAngle(float angle) { _targetAngle = angle; _isKnob = true; }
        // constant
        //float getTargetAngle() { return _targetAngle; }
    

        // is constant possible?  (getClosestPoint is problem)
        //bool insideContour (float x, float y);
        bool contains(float x, float y) const;
    
        virtual void draw();
    
        void drawTechnical();
    
        // lastTriggered etc.
    
    
};