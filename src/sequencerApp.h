#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"

#include "STImplementation.h"
#include "displayModes.h"
#include "displayModeBase.h"
#include "dmFeeder.h"
#include "dmFirstBlobResult.h"
#include "dmTechnicalView.h"
#include "dmDetectColors.h"
#include "dmPresent.h"

class SequencerApp : public ofBaseApp {
	public:
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
        // ------------------------------------
    

        STImplementation *stImplementation;
    
    
        // ------------------------------------
    
        void setMode(DisplayModes mode);
    
        DisplayModes currentDisplayMode = DM_UNDEFINED;
    
        DisplayModeBase *displayMode;
    
        DMFeeder *displayModeFeeder;
        DMFirstBlobResult *displayModeFirstBlobResult;
        DMTechnicalView *displayModeTechnicalView;
        DMDetectColors *displayModeDetectColors;
        DMPresent *displayModePresent;
    
    
};


