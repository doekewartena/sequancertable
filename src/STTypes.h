//
//  STTypes.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/28/13.
//
//

#pragma once

#ifndef SOUND_TANGIBLE_TYPES
#define SOUND_TANGIBLE_TYPES

enum STTypes {
    UNDEFINED,
    SEQUENCER,
    SAMPLE,
    SYNTHESIZER,
    BREAK,
    MUTE,
    UNMUTE,
    GROUP,
    DELAY
};

#endif


