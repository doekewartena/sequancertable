//
//  ofxDTangibleBase.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/29/13.
//
//

#include "ofxDTangibleBase.h"
#include "ofxDGlobalMethods.h"




// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
/*
bool ofxDTangibleBase::operator==(const ofxDTangibleBase &other) const {
    
    // not done atm (and probably not really needed): vector <ofPoint>    pts;
    return (
            area == other.area &&
            length == other.length &&
            boundingRect == other.boundingRect &&
            centroid == other.centroid &&
            hole == other.hole &&
            nPts == other.nPts &&
            
            // tangible variables
            
            //_color == other._color &&
            //_colorIsSet == other._colorIsSet &&
            _angleDetected == other._angleDetected &&
            _angle == other._angle //&&
            //_targetAngle == other._targetAngle
            
            
            
            );
    
}
 */

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

/*
bool ofxDTangibleBase::insideContour(float px, float py) {
    // first check rectangle
    if (boundingRect.inside(px, py)) {
        
        // now get the closest point and see if the distance from
        // px,py to the centroid is closer then the closest
        // point to the centroid
        ofPoint &closest = getClosestPoint(pts, px, py);
        float d = ofDistSquared(closest.x, closest.y, centroid.x, centroid.y);
        float d2 = ofDistSquared(px, py, centroid.x, centroid.y);
        
        return d2 < d;
        
    }
    return false;
}
*/

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

/**
 ellipse based check
 */
bool ofxDTangibleBase::contains(float x, float y) const {
    
    float half_w = boundingRect.width/2;
    float half_h = boundingRect.height/2;
    float dx = (x-boundingRect.getCenter().x)/half_w;
    float dy = (y-boundingRect.getCenter().y)/half_h;
    return dx*dx+dy*dy <= 1;
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void ofxDTangibleBase::draw() {
    
    //ofSetColor(255);
    //thresholdImage->draw(boundingRect.x - thresholdImageOffset.x, boundingRect.y - thresholdImageOffset.y);
    //thresholdImage->setROI(thresholdImageOffset.x, thresholdImageOffset.y, boundingRect.width, boundingRect.height);
    
    // draw detectColor
    //ofFill();
    ofSetColor(detectColor->color);
    //ofEllipse(centroid.x, centroid.y, 20, 20);
    
    thresholdImage->drawROI(boundingRect.x, boundingRect.y);
    
    
    // draw subsection
    
    ofxCvBlob::draw();
    
    bool mouseInside = contains(ofGetMouseX(), ofGetMouseY());
    
    if (mouseInside) {
        ofNoFill();
        ofSetHexColor(0xff0099);
        ofLine(boundingRect.x, boundingRect.y, boundingRect.getMaxX(), boundingRect.getMaxY());
        ofLine(boundingRect.getMaxX(), boundingRect.y, boundingRect.x, boundingRect.getMaxY());
    }
    /*
    if (colorIsSet()) {
        // draw the color
        float s = min(boundingRect.width, boundingRect.height);
        s = insideCountour ? s *= 0.5 : s *= 0.25;
        ofFill();
        ofSetColor(_color);
        ofEllipse(centroid.x, centroid.y, s, s);
    }
     */


    
    if(angleDetected()) {
        ofNoFill();
        ofSetColor(0, 255, 0);
        ofSetLineWidth(1);
        float m = MAX(boundingRect.width, boundingRect.height);
        ofLine(centroid.x, centroid.y, centroid.x + cos(_angle) * m, centroid.y + sin(_angle) * m);
        /*
        ofSetColor(255, 0, 0);
        ofLine(centroid.x, centroid.y, centroid.x + cos(_targetAngle) * 100, centroid.y + sin(_targetAngle) * 100);
        */
        
        ofFill();
        ofSetColor(0,255,0);
        for (int i = ptsAngleInfoFirst.startIndex; i < ptsAngleInfoFirst.endIndex; i++) {
            ofRect(pts[i].x, pts[i].y, 2, 2);
        }
        ofSetColor(0,0,255);
        for (int i = ptsAngleInfoSecond.startIndex; i < ptsAngleInfoSecond.endIndex; i++) {
            ofRect(pts[i].x, pts[i].y, 2, 2);
        }
        
    }
    
    // draw it's classname
    if (mouseInside) {
        ofSetColor(255,0,0);
        ofDrawBitmapString(className, boundingRect.x, boundingRect.y);
        ofEllipse(centroid.x, centroid.y, 10, 10);
    }

    
    
    
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


void ofxDTangibleBase::drawTechnical() {
    ofxDTangibleBase::draw();
}






