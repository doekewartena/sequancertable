//
//  ofxDDetectColor.h
//  sequancerTable
//
//  Created by Doeke Wartena on 11/6/13.
//
//

#pragma once
#include "ofMain.h"


struct ofxDDetectColor {
    
    ofColor color;
    const char *name;
    
    // should be normalized
    float fuzziness = 0.5;
    
    float incrementStep = 0.01;
    
    ofxDDetectColor(ofColor color, const char *name, float fuzziness) {
        this->color = color;
        this->name = name;
        this->fuzziness = fuzziness;
    }
    
    
    // . . . . . . . . . . . . . . . .
    
    void incrementFuzziness() {
        fuzziness += incrementStep;
        constrainFuzziness();
    }
    
    // . . . . . . . . . . . . . . . .
    
    void incrementFuzziness(float incrementStep) {
        fuzziness += incrementStep;
        constrainFuzziness();
    }
    
    // . . . . . . . . . . . . . . . .
    
    void decrementFuzziness() {
        fuzziness -= incrementStep;
        constrainFuzziness();
    }
    
    // . . . . . . . . . . . . . . . .
    
    void decrementFuzziness(float decrementStep) {
        fuzziness -= abs(decrementStep);
        constrainFuzziness();
    }
    
    // . . . . . . . . . . . . . . . .
    
    void setFuzziness(float value) {
        fuzziness = value;
        constrainFuzziness();
    }
    
    // . . . . . . . . . . . . . . . .
    
    void constrainFuzziness() {
        if (fuzziness > 1) fuzziness = 1;
        else if (fuzziness < 0) fuzziness = 0;
    }
    
    
};