//
//  ofxDGlobalMethods.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/26/13.
//
//

#include "ofxDGlobalMethods.h"



// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

float dist(const ofxDTangibleBase &a, const ofxDTangibleBase &b) {
    return ofDist(a.centroid.x, a.centroid.y, b.centroid.x, b.centroid.y);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

float dist(const ofxDTangibleBase *a, const ofxDTangibleBase *b) {
    return ofDist(a->centroid.x, a->centroid.y, b->centroid.x, b->centroid.y);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

float dist(float x, float y, const ofxDTangibleBase &t) {
    return ofDist(x, y, t.centroid.x, t.centroid.y);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

float distSquared(const ofxDTangibleBase &a, const ofxDTangibleBase &b) {
    return ofDistSquared(a.centroid.x, a.centroid.y, b.centroid.x, b.centroid.y);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

float distSquared(const ofxDTangibleBase *a, const ofxDTangibleBase *b) {
    return ofDistSquared(a->centroid.x, a->centroid.y, b->centroid.x, b->centroid.y);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

float distSquared(float x, float y, const ofxDTangibleBase &t) {
    return ofDistSquared(x, y, t.centroid.x, t.centroid.y);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


ofxDTangibleBase* getClosestTangible(int x, int y, vector<ofxDTangibleBase> &searchList, bool notEqualToSearchPosition) {
    
    if(searchList.size() == 0) {
        printf("WARNING: before calling getClosestTangible, make sure the searchList contains any objects!\n");
        return NULL;
    }
    
    float closestDist = FLT_MAX;
    int index = -1;
    
    for(int i = 0; i < searchList.size(); i++) {
        ofxDTangibleBase &t = searchList.at(i);
        
        // TODO, check with and height etc?
        if(t.centroid.x == x && t.centroid.y == y) {
            if(notEqualToSearchPosition == false) {
                // Indirection requires pointer operand
                //return t;
                //return searchList.at(i);
                ofxDTangibleBase* tt = &t;
                return tt;
            }
            else {
                continue;
            }
        }
        
        float d = ofDistSquared(x, y, t.centroid.x, t.centroid.y);
        
        if(d < closestDist) {
            if(d == 0) {
                //return ofxDTangibleBase(*t);
                ofxDTangibleBase* tt = &t;
                return tt;
            }
            closestDist = d;
            index = i;
        }
        
    }
    
    //return searchList.at(index);
    ofxDTangibleBase* tt = &searchList.at(index);
    return tt;
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


ofxDTangibleBase* getClosestTangible(const ofPoint *centroid, vector<ofxDTangibleBase> &searchList, bool notEqualToSearchPosition) {
    
    return getClosestTangible(centroid->x, centroid->y, searchList, notEqualToSearchPosition);
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

// TODO const?
ofxDTangibleBase* getClosestTangible(const ofxDTangibleBase *tangible, vector<ofxDTangibleBase> &searchList, bool notEqualToSearchPosition) {
    
    return getClosestTangible(tangible->centroid.x, tangible->centroid.y, searchList, notEqualToSearchPosition);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

ofxDTangibleBase* getClosestTangible(int x, int y, const vector<ofxDTangibleBase*> &searchList, bool notEqualToSearchPosition) {
    
    if(searchList.size() == 0) {
        printf("WARNING: before calling getClosestTangible, make sure the searchList contains any objects!\n");
        return NULL;
    }
    
    float closestDist = FLT_MAX;
    int index = -1;
    
    for(int i = 0; i < searchList.size(); i++) {
        ofxDTangibleBase *t = searchList.at(i);
        
        // TODO, check with and height etc?
        if(t->centroid.x == x && t->centroid.y == y) {
            if(notEqualToSearchPosition == false) {
                return t;
            }
            else {
                continue;
            }
        }
        
        float d = ofDistSquared(x, y, t->centroid.x, t->centroid.y);
        
        if(d < closestDist) {
            if(d == 0) {
                return t;
            }
            closestDist = d;
            index = i;
        }
        
    }
    
    return searchList.at(index);
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


ofxDTangibleBase* getClosestTangible(const ofPoint *centroid, const vector<ofxDTangibleBase*> &searchList, bool notEqualToSearchPosition) {
    
    return getClosestTangible(centroid->x, centroid->y, searchList, notEqualToSearchPosition);
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


ofxDTangibleBase* getClosestTangible(const ofxDTangibleBase *tangible, const vector<ofxDTangibleBase*> &searchList, bool notEqualToSearchPosition) {
    
    return getClosestTangible(tangible->centroid.x, tangible->centroid.y, searchList, notEqualToSearchPosition);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


bool inOval(float x, float y, float ovalX, float ovalY, float w, float h) {
    w = w/2;
    h = h/2;
    float dx = (x-ovalX)/w;
    float dy = (y-ovalY)/h;
    if (dx*dx+dy*dy <= 1) return true;
    return false;
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool ovalIntersectsOval(float cx1, float cy1, float w1, float h1, float cx2, float cy2, float w2, float h2) {
    
    // find the angle from 1 to 2
    float a = atan2(cy2 - cy1, cx2 - cx1);
    
    float x = (w1/2 * h1/2) / sqrt(pow(h1/2, 2) + pow(w1/2, 2)* pow(tan(a), 2));
    if (abs(a) >= PI/2) {
        x *= -1;
    }
    float y = tan(a)*x;
    
    return inOval(x+cx1, y+cy1, cx2, cy2, w2, h2);
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


ofPoint& getClosestPoint(vector <ofPoint> &pts, float px, float py)  {
    
    assert(pts.size() > 0 && "getClosestPoint: make sure array size is > 0");
    
    int closestIndex = 0;
    float closestDist = FLT_MAX;
    float d;
    
    for (int i = 0; i < pts.size(); i++) {
        ofPoint &p = pts[i];
        d = ofDistSquared(p.x, p.y, px, py);
        if (d < closestDist) {
            closestDist = d;
            closestIndex = i;
        }
    }
    return pts[closestIndex];
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

// 'Pointer' declared as a pointer to a reference of type 'ofxDTangibelBase &'

vector<ofxDTangibleBase*> closest2 (vector<ofxDTangibleBase> &listA, vector<ofxDTangibleBase> &listB) {
    
    vector<ofxDTangibleBase*> returnList;
    
    float closestDistance = FLT_MAX;
    ofxDTangibleBase* closestA = NULL;
    ofxDTangibleBase* closestB = NULL;
    
    for (ofxDTangibleBase &a : listA) {
        for (ofxDTangibleBase &b : listB) {
            float d = distSquared(a, b);
            
            if ( d == 0) {
                ofxDTangibleBase* ptrA = &a;
                ofxDTangibleBase* ptrB = &b;
                returnList.push_back(ptrA);
                returnList.push_back(ptrB);
                return returnList;
            }
            
            if (d < closestDistance) {
                closestDistance = d;
                closestA = &a;
                closestB = &b;
            }
            
        }
    }
    
    returnList.push_back(closestA);
    returnList.push_back(closestB);
    
    return returnList;
    
    
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

vector<ofxDTangibleBase*> closest2 (const  vector<ofxDTangibleBase*> &listA, const vector<ofxDTangibleBase*> &listB) {
    
    vector<ofxDTangibleBase*> returnList;
    
    float closestDistance = FLT_MAX;
    ofxDTangibleBase* closestA = NULL;
    ofxDTangibleBase* closestB = NULL;
    
    for (int i = 0; i < listA.size(); i++) {
        ofxDTangibleBase *a = listA[i];
        
        for (int j = 0; j < listB.size(); j++) {
            ofxDTangibleBase *b = listB[j];
        
            float d = distSquared(a, b);
            
            if ( d == 0) {
                // we can't have a distance closer then 0
                returnList.push_back(a);
                returnList.push_back(b);
                return returnList;
            }
            
            if (d < closestDistance) {
                closestDistance = d;
                closestA = a;
                closestB = b;
            }
            
        }
    }
    
    returnList.push_back(closestA);
    returnList.push_back(closestB);
    
    return returnList;
    
    
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

vector<shared_ptr<ofxDTangibleBase> > closest2 (const vector<shared_ptr<ofxDTangibleBase> > &listA, const vector<shared_ptr<ofxDTangibleBase> > &listB) {
    
    vector<shared_ptr<ofxDTangibleBase> > returnList;
    
    float closestDistance = FLT_MAX;
    shared_ptr<ofxDTangibleBase> closestA;
    shared_ptr<ofxDTangibleBase> closestB;
    
    for (int i = 0; i < listA.size(); i++) {
        shared_ptr<ofxDTangibleBase> a = listA[i];
        
        for (int j = 0; j < listB.size(); j++) {
            shared_ptr<ofxDTangibleBase> b = listB[j];
            
            float d = distSquared(a.get(), b.get());
            
            if ( d == 0) {
                // we can't have a distance closer then 0
                returnList.push_back(a);
                returnList.push_back(b);
                return returnList;
            }
            
            if (d < closestDistance) {
                closestDistance = d;
                closestA = a;
                closestB = b;
            }
            
        }
    }
    
    returnList.push_back(closestA);
    returnList.push_back(closestB);
    
    return returnList;
    
    
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


// returns a normalized where 1 is an exact match
// BAD METHOD?, it's more complicated?

float colorSimilarity (ofColor ca, ofColor cb) {
    
    float r = 255 - abs(ca.r - cb.r);
    float g = 255 - abs(ca.g - cb.g);
    float b = 255 - abs(ca.b - cb.b);
    
    return (r + g + b) / 765;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .




// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

// returns a normalized where 1 is an exact match
// BAD METHOD, it's more complicated
/*
float colorSimilarity (unsigned char r1, unsigned char g1, unsigned char b1, unsigned char r2, unsigned char g2, unsigned char b2) {
    
    float r = 255 - abs(r1 - r2);
    float g = 255 - abs(g1 - g2);
    float b = 255 - abs(b1 - b2);
    
    return (r + g + b) / 765;
}
*/
// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .



