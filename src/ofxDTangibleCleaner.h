//
//  ofxDTangibleCleaner.h
//  sequancerTable
//
//  Created by Doeke Wartena on 11/27/13.
//
//

# pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxDTangibleBase.h"

class ofxDTangibleCleaner {
    
    
    public:
        // this is a circle of normilized vectors
        // which is put on top of a tangible to get
        // the colors at the specific spots
        vector<ofVec2f> colorDetectionVecs;
        vector<shared_ptr<ofxDTangibleBase> > _tangibles;

        // constructor
        ofxDTangibleCleaner() {
            createColorDetectionPoints(32);
        }
        
        // returns how many points are actually created
        int createColorDetectionPoints(int nTargetPoints);
        
        void clearOverlappingTangibles(const vector<shared_ptr<ofxDTangibleBase> > &tangibles, ofxCvColorImage &img);
        void detectColor(ofxDTangibleBase &t, ofxCvColorImage &img);
    
        const vector<shared_ptr<ofxDTangibleBase> > &getTangibles() const { return _tangibles;}

    
    
    
};
