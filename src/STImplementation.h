//
//  STImplementation.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/30/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxDTangibleBase.h"
#include "STTypes.h"
#include "ofxDGlobalMethods.h"
#include "STSample.h"
#include "tangibleFeeder.h"
#include "ofxDTangibleFinder.h"
#include "ofxDTangibleManager.h"
#include "ofxDTangibleCleaner.h"
#include "ofxOsc.h"

#define HOST "localhost"
#define PORT 12345


class STImplementation {
    
    public:
    
    
        TangibleFeeder *feeder;
        
        ofxDTangibleFinder *tangibleFinder;
        ofxDKnobAngleDetector *knobAngleDetector;
        ofxDTangibleCleaner *tangibleCleaner;
        ofxDTangibleManager *tangibleManager;
    
        vector<ofxDDetectColor*> detectColors;
    
        STImplementation();
    
        void update();
    
        void convertToAppropriateType(const vector<shared_ptr<ofxDTangibleBase> > &tangibles);
    
        ofxOscSender sender;

    
};