//
//  screenFeed.h
//  sequancerTable
//
//  Created by Doeke Wartena on 11/4/13.
//
//

#pragma once

#include "ofMain.h"
#include "tangibleFeeder.h"
#include "ofxDisplay.h"

class ScreenFeed : public TangibleFeeder {
    
    public:
    
        ofxDisplay *display;
    
        // constructors
        ScreenFeed();
        ScreenFeed(CGDirectDisplayID displayId);
    
        // overwrite methods
        bool hasUpdate();
        void updateColorImage();
        
        ofImage image;
    
        
};
