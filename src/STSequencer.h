//
//  STSequencer.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/28/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxDTangibleBase.h"
#include <time.h>


class STSequencer : public ofxDTangibleBase {
    
    public:
    
    
        float radius = 400;
        // rounds per second
        float rps = 0.5;
        // we save the time after each update
        clock_t lastUpdate;
        // radians
        float angleRadar = 0;

    
    
        STSequencer(ofxDTangibleBase& base) : ofxDTangibleBase(base, "STSequencer") {
            
        }
    
        void draw();
    
    
};