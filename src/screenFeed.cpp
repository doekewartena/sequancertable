//
//  screenFeed.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 11/4/13.
//
//

#include "screenFeed.h"




ScreenFeed::ScreenFeed() {
    
    display = ofxDisplay::getMainDisplay();
    
    
    ofRectangle rect = display->getBounds();
    
    _feedWidth = rect.width;
    _feedHeight = rect.height;
    
    image.allocate(_feedWidth, _feedHeight, OF_IMAGE_COLOR);
    
    init();
    
}


// . . . . . . . . . . . . . . . . . . . . . . . . .


ScreenFeed::ScreenFeed(CGDirectDisplayID displayId) {
    
    display = new ofxDisplay(displayId);
    
    ofRectangle rect = display->getBounds();
    
    _feedWidth = rect.width;
    _feedHeight = rect.height;
    
    image.allocate(_feedWidth, _feedHeight, OF_IMAGE_COLOR);
    
    
    init();

}


// . . . . . . . . . . . . . . . . . . . . . . . . .



bool ScreenFeed::hasUpdate() {
    return true;
}



// . . . . . . . . . . . . . . . . . . . . . . . . .


void ScreenFeed::updateColorImage() {

    display->grabImage(image);
    
    if (image.getPixels() != NULL) {
        colorImg.setFromPixels(image.getPixels(), _feedWidth, _feedHeight);
    }
    
    
}



// . . . . . . . . . . . . . . . . . . . . . . . . .