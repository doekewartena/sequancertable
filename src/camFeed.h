//
//  camFeed.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/24/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "tangibleFeeder.h"

class CamFeed : public TangibleFeeder {
public:
    // constructor
    CamFeed(int deviceId, int width, int height, int frameRate); //
    // overwrite methods
    bool hasUpdate();
    void updateColorImage();
    
    // variables
    ofVideoGrabber vidGrabber;
    
    
};
