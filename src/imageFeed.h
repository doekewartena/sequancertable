//
//  imageFeed.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/25/13.
//
//

#pragma once

#include "ofMain.h"
#include "tangibleFeeder.h"

class ImageFeed : public TangibleFeeder {
public:
    // constructor
    ImageFeed(string tangibleImage, string backgroundImage);
    // overwrite methods
    bool hasUpdate();
    void updateColorImage();
    
    //
    //vector<ofImage> images;
    
    
};
