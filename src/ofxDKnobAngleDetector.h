//
//  knobAngleDetector.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/15/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxDTangibleBase.h"


class ofxDKnobAngleDetector {
    
    private:
        float fAngle;
    
        vector<shared_ptr<ofxDTangibleBase> > _tangibles;
    
    

    public:
    
        // ignore all contour points outside this range
        float maxRad = 0.8;
        float minRad = 0.4;

        // constructor
        //ofxDKnobAngleDetector();

        
        void detect(const vector<shared_ptr<ofxDTangibleBase> > &tangibles);
        bool detect(ofxDTangibleBase &tangible);
    
        float angle() { return fAngle; }
    
        const vector<shared_ptr<ofxDTangibleBase> > &getTangibles() const { return _tangibles;}
    

    

};

