//
//  ofxTangibleManager.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/24/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxDTangibleBase.h"


class ofxDTangibleManager {
    
    private:
        // within this dist we assume it's found again
        float maxMoveDist = 20;
        float maxMoveDistSquared = 20*20;
    
    public:
    
        void setMaxMoveDist(float d) { maxMoveDist = d; maxMoveDistSquared = maxMoveDist*maxMoveDist; }
        float getMaxMoveDist() const { return maxMoveDist; }

        // updates global tangibles with the new list
        void updateList(const vector<shared_ptr<ofxDTangibleBase> > oldList, const vector<shared_ptr<ofxDTangibleBase> > &newList);
    


    
};