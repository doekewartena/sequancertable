//
//  ofxDGlobalTangibleList.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/29/13.
//
//

#include "ofxDGlobalTangibleList.h"
#include "ofxDGlobalMethods.h"

ofxDGlobalTangibleList gTangibleList;






// . . . . . . . . . . . . . . . . . . . . . . . . . . .


void ofxDGlobalTangibleList::addTangible(shared_ptr<ofxDTangibleBase> sp) {
    
    ofxDTangibleBase *t = sp.get();
    
    if (t->inGlobalList) {
        printf("WARNING: ofxDGlobalTangibleList::addTangible: tangible allready in global list\n");
    }
    
    if (_tangibles.size() > 0) {
        ofxDTangibleBase *last = _tangibles.at(_tangibles.size()-1).get();
    
        last->next = t;
        t->previous = last;
    }
    
    t->inGlobalList = true;
    
    _tangibles.push_back(sp);
    
    printf("added tangible of type: %s\n", t->className);
    
    
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . .

void ofxDGlobalTangibleList::deleteTangible(shared_ptr<ofxDTangibleBase> sp) {
    
    ofxDTangibleBase *t = sp.get();
    
    if (!t->inGlobalList) {
        printf("WARNING: ofxDGlobalTangibleList::deleteTangible tangible not in global list\n");
    }
    
    for (int i = 0; i < _tangibles.size(); i++) {
        ofxDTangibleBase* ptr = _tangibles[i].get();
        
        if (ptr == t) {
            
            // relink the chain
            if (t->previous != NULL) {
                if (t->next != NULL) {
                    t->previous->next = t->next;
                    t->next->previous = t->previous;
                }
                else {
                    // removing the last element
                    t->previous->next = NULL;
                }
            }
            else {
                // could be it was the first element we remove
                if (t->next != NULL) {
                    t->next->previous = NULL;
                    t->next = NULL;
                }
            }
            
            //t->inGlobalList = false;
            //_tangibles.erase(_tangibles.begin()+i);
            // Type of 'vector< *>' does not provide a call operator
            _tangibles.erase(remove(_tangibles.begin(), _tangibles.end(), sp));
            
            //delete t;
            
            return;
        }
    }
}
// . . . . . . . . . . . . . . . . . . . . . . . . . . .


void ofxDGlobalTangibleList::addTangible(ofxDTangibleBase *t) {
    
    shared_ptr<ofxDTangibleBase> sp(t);
    addTangible(sp);
    
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . .

void ofxDGlobalTangibleList::deleteTangible(ofxDTangibleBase *t) {
    
    if (!t->inGlobalList) {
        printf("WARNING: ofxDGlobalTangibleList::deleteTangible tangible not in global list\n");
    }
    
    for (int i = _tangibles.size()-1; i >= 0; i--) {
        shared_ptr<ofxDTangibleBase> sp = _tangibles[i];
        if (t == sp.get()) {
            deleteTangible(sp);
            return;
        }
    }
    

}



// . . . . . . . . . . . . . . . . . . . . . . . . . . .


void ofxDGlobalTangibleList::deleteAll() {
    /*
    for (int i = 0; i < _tangibles.size(); i++) {
        ofxDTangibleBase *t = _tangibles[i];
        delete t;
    }
     */
    // shared pointers now so easy
    _tangibles.clear();
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . .


ofxDTangibleBase* ofxDGlobalTangibleList::findTangibleByClassName(ofxDTangibleBase* start, const char *className) {
    

    if (_tangibles.size() == 0) {
        return NULL;
    }
    
    ofxDTangibleBase* current = start;
    
    if (!current) {
        current = _tangibles[0].get();
        if (current->className == className) {
            return current;
        }
    }
    
    
    while (current != NULL) {
        if (current->next == NULL) {
            return NULL;
        }
        if (current->next->className == className) {
            return current->next;
        }
        current = current->next;
    }
    
    return NULL;
    
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . .


ofxDTangibleBase* ofxDGlobalTangibleList::findTangibleWithinRadius(ofxDTangibleBase* center, ofxDTangibleBase* start, float radius) {
    
    if (_tangibles.size() == 0) {
        return NULL;
    }
    
    ofxDTangibleBase* current = start;
    
    if (!current) {
        current = _tangibles[0].get();
        if ( current != center) {
            if (dist(center, current) <= radius ) {
                return current;
            }
        }
    }
    
    while (current != NULL) {
        if (current->next == NULL) {
            return NULL;
        }
        current = current->next;
        if ( current != center) {
            if (dist(center, current) <= radius ) {
                return current;
            }
        }
    }

    return NULL;
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . .


ofxDTangibleBase* ofxDGlobalTangibleList::findTangibleWithinRadiusByClassName(ofxDTangibleBase* center, ofxDTangibleBase* start, float radius, const char *className) {
    
    
    if (_tangibles.size() == 0) {
        return NULL;
    }
    
    ofxDTangibleBase* current = start;
    
    if (!current) {
        current = _tangibles[0].get();
        if ( current != center) {
            if (current->className == className) {
                if (dist(center, current) <= radius ) {
                    return current;
                }
            }
        }
    }
    
    while (current != NULL) {
        if (current->next == NULL) {
            return NULL;
        }
        current = current->next;
        if ( current != center) {
            if (current->className == className) {
                if (dist(center, current) <= radius ) {
                    return current;
                }
            }
        }
    }
    
    return NULL;

}


// . . . . . . . . . . . . . . . . . . . . . . . . . . .