//
//  dmPresent.h
//  sequancerTable
//
//  Created by Doeke Wartena on 12/3/13.
//
//

#pragma once

#include "ofMain.h"
#include "displayModeBase.h"
#include "STImplementation.h"


class DMPresent : public DisplayModeBase {
    
    
public:
    
    TangibleFeeder *feeder;
    
    DMPresent(STImplementation *i) : DisplayModeBase(i) {
        feeder = impl->feeder;
    }
    
    void update();
    void draw();
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    void focus();
    void unfocus();
    
    
};
