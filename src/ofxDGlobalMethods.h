//
//  ofxDGlobalMethods.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/26/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxDTangibleBase.h"
//#include "ofxDGlobalTangibleList.h"

#ifndef OFXD_GLOBAL_METHODS
#define OFXD_GLOBAL_METHODS
float dist(const ofxDTangibleBase &a, const ofxDTangibleBase &b);
float dist(const ofxDTangibleBase *a, const ofxDTangibleBase *b);
float dist(float x, float y, const ofxDTangibleBase &t);
float distSquared(const ofxDTangibleBase &a, const ofxDTangibleBase &b);
float distSquared(const ofxDTangibleBase *a, const ofxDTangibleBase *b);
float distSquared(float x, float y, const ofxDTangibleBase &t);
// make const? -> drops qualifier, look into this later
ofxDTangibleBase* getClosestTangible (int x, int y, vector<ofxDTangibleBase> &searchList, bool notEqualToSearchPosition);
ofxDTangibleBase* getClosestTangible (ofPoint *centroid, vector<ofxDTangibleBase> &searchList, bool notEqualToSearchPosition);
ofxDTangibleBase* getClosestTangible (ofxDTangibleBase *tangible, vector<ofxDTangibleBase> &searchList, bool notEqualToSearchPosition);

ofxDTangibleBase* getClosestTangible (int x, int y, const vector<ofxDTangibleBase*> &searchList, bool notEqualToSearchPosition);
ofxDTangibleBase* getClosestTangible (ofPoint *centroid, const vector<ofxDTangibleBase*> &searchList, bool notEqualToSearchPosition);
ofxDTangibleBase* getClosestTangible (ofxDTangibleBase *tangible, const vector<ofxDTangibleBase*> &searchList, bool notEqualToSearchPosition);

bool inOval(float x, float y, float ovalX, float ovalY, float w, float h);
bool ovalIntersectsOval(float cx1, float cy1, float w1, float h1, float cx2, float cy2, float w2, float h2);
// make const?
ofPoint& getClosestPoint(vector <ofPoint> &pts, float px, float py);
// make const?
vector<ofxDTangibleBase*> closest2 (vector<ofxDTangibleBase> &listA, vector<ofxDTangibleBase> &listB);
vector<ofxDTangibleBase*> closest2 (const vector<ofxDTangibleBase*> &listA, const vector<ofxDTangibleBase*> &listB);
vector<shared_ptr<ofxDTangibleBase> > closest2 (const vector<shared_ptr<ofxDTangibleBase> > &listA, const vector<shared_ptr<ofxDTangibleBase> > &listB);
// make const?
float colorSimilarity (ofColor source, ofColor target);
//float colorSimilarity (unsigned char r1, unsigned char g1, unsigned char b1, unsigned char r2, unsigned char g2, unsigned char b2);
#endif