//
//  STImplementation.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/30/13.
//
//

#include "STImplementation.h"
#include "ofxDGlobalTangibleList.h"
#include "imageBufferFeed.h"
#include "screenFeed.h"
#include "imageFeed.h"
#include "STSequencer.h"
#include "STSample.h"
#include "camFeed.h"
#include "ofxDDetectColor.h"

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


STImplementation::STImplementation () {
    
    // choose one
    feeder = new CamFeed(1, 1280, 960, 30);
    //feeder = new ImageFeed("testImageCleanColor.png", "testImageBackground.png");
    //feeder = new ImageFeed("testImageColor.png", "testImageBackground.png");
    //feeder = new ImageFeed("testImageColor2x.png", "testImageBackground.png");
    //feeder = new ImageFeed("testImageColor1x.png", "testImageBackground.png");
    //feeder = new ImageFeed("testImageStress1.png", "testImageBackground.png");
    //feeder = new ImageFeed("testFrames/frame160.png", "testFrames/frame1.png");
    // bufferFeed is a litle hard coded for now
    //feeder = new ImageBufferFeed("testFrames/frame1.png");
    
    /*
     static vector<ofxDisplay*> displays = ofxDisplay::getActiveDisplays();
     
     for(int i = 0; i < displays.size(); i++) {
     ofxDisplay *display = displays[i];
     ofRectangle bounds = display->getBounds();
     printf("display %i width: %f height: %f\n", i, bounds.width, bounds.height);
     
     }
     
     
     feeder = new ScreenFeed();
     */
    
    feeder->setThreshold(100);
    
    tangibleFinder = new ofxDTangibleFinder();
    
    tangibleFinder->setMinDiameter(45);
    tangibleFinder->setMaxDiameter(60);
    tangibleFinder->setMaxTangibles(50);
    tangibleFinder->setMaxAspectDeviation(0.2);
    tangibleFinder->setMaxCentroidDeviation(0.3);
    
    knobAngleDetector = new ofxDKnobAngleDetector();
    
    tangibleCleaner = new ofxDTangibleCleaner();
    
    tangibleManager = new ofxDTangibleManager();
    tangibleManager->setMaxMoveDist(50);
    
    
    ofxDDetectColor* dc = new ofxDDetectColor(ofColor(113, 34, 37), "red", 0.1); // 0.1 -- use 0.2 for overlap with magenta
    detectColors.push_back(dc);
    
    dc = new ofxDDetectColor(ofColor(0, 61, 49), "green", 0.07);
    detectColors.push_back(dc);
    
    dc = new ofxDDetectColor(ofColor(1, 20, 54), "blue", 0.05);
    detectColors.push_back(dc);
    
    dc = new ofxDDetectColor(ofColor(116, 122, 70), "yellow", 0.08);
    detectColors.push_back(dc);
    
    dc = new ofxDDetectColor(ofColor(162, 40, 65), "magenta", 0.17); // 0.17 -- use 0.2  for overlap with red
    detectColors.push_back(dc);
    
    
    sender.setup(HOST, PORT);
    
    
    
}



// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


void STImplementation::update() {
    
    if(feeder->hasUpdate()) {
        
        //printf("framenum: %i\n", ofGetFrameNum());
        
        feeder->updateColorImage();
        
        tangibleFinder->findTangibles(feeder->getTangibleImage(), feeder->colorImg, detectColors);
        // now tangibleFinder can hold a lot of tangibles
        // this can also mean overlapping ones by colors that come close
        // or garbage that succeeded as a tangible
        knobAngleDetector->detect(tangibleFinder->getTangibles());
        // knobAngleDetector holds now only the tangibles that
        // passed the test to find an angle on it
        // however it can still hold overlapping tangibles
        tangibleCleaner->clearOverlappingTangibles(knobAngleDetector->getTangibles(), feeder->colorImg);
        // now we have a prety clear list
        tangibleManager->updateList(gTangibleList.getTangibles(), tangibleCleaner->getTangibles());
        
        convertToAppropriateType(gTangibleList.getTangibles());
        
    }
    
    
    /*
    // we want to update the playing of the sound independent
    // of if we have a feeder update or not
    // NOTE that this means that if the feeder get's lost the
    // sound will start looping
    
    // without a sequencer there is no sound, so get the sequencers first
    ofxDTangibleBase *bSequencer = gTangibleList.findTangibleByClassName(NULL, "STSequencer");
    
    while (bSequencer) {
        
        STSequencer *sequencer = static_cast<STSequencer*>(bSequencer);
        
        // TODO
        // check the speed of sequencer
        // if it's 0 there is no need to update
        
        
        // get everything WE WANT within the radius of the sequencer
        // first handle samples
        ofxDTangibleBase *bSample = gTangibleList.findTangibleWithinRadiusByClassName(sequencer, NULL, sequencer->radius ,"STSample");
        
        while (bSample) {
            STSample *sample = static_cast<STSample*>(bSample);
            
            //
            
            
            bSample = gTangibleList.findTangibleWithinRadiusByClassName(sequencer, bSample, sequencer->radius ,"STSample");
        }
        
        
        
        // find the next one
        bSequencer = gTangibleList.findTangibleByClassName(bSequencer, "STSequencer");
    }
    */
    
    ofxOscMessage m;
	m.setAddress("/mouse/position");
	m.addIntArg(666);
	m.addIntArg(999);
	sender.sendMessage(m);
    
    

}




// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void STImplementation::convertToAppropriateType(const vector<shared_ptr<ofxDTangibleBase> > &tangibles) {
    
    
    for (int i = tangibles.size()-1; i >= 0; i--) {
        shared_ptr<ofxDTangibleBase> sp = tangibles[i];
        
        ofxDTangibleBase *t = sp.get();
        
        // when we founded the tangibles we passed
        // the detectColors
        // we use those now to determine which type
        // it should be
        
        
        // SEQUENCER
        if (strcmp(t->detectColor->name, "yellow") == 0) {
            // check if it was allready a sequencer
            if (strcmp(t->className, "STSequencer") == 0) {
                continue;
            }
            STSequencer *sequencer = new STSequencer(*t);
            gTangibleList.deleteTangible(sp);
            gTangibleList.addTangible(sequencer);
        }
        // SAMPLE
        else if (strcmp(t->detectColor->name, "red") == 0) {
            // check if it was allready a sample
            if (strcmp(t->className, "STSample") == 0) {
                continue;
            }
            STSample *sample = new STSample(*t);
            gTangibleList.deleteTangible(sp);
            gTangibleList.addTangible(sample);
        }
        // TODO
        // other tangible types
        

    
        
    }
    
    
}