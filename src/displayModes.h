//
//  displayModes.h
//  sequancerTable
//
//  Created by Doeke Wartena on 11/5/13.
//
//

#pragma once

enum DisplayModes {
    DM_UNDEFINED,
    DM_FEEDER,
    DM_FIRST_BLOB_RESULT,
    DM_TECHNICAL_VIEW,
    DM_DETECT_COLORS,
    DM_PRESENT
};