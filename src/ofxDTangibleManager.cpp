//
//  ofxTangibleManager.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/24/13.
//
//

#include "ofxDTangibleManager.h"
#include "ofxDGlobalMethods.h"
#include "ofxDGlobalTangibleList.h"
//#include <time.h>



void ofxDTangibleManager::updateList(const vector<shared_ptr<ofxDTangibleBase> > oldList, const vector<shared_ptr<ofxDTangibleBase> > &newList) {
    
    
    //clock_t tStart = clock();
    

    
    // if there is no new list
    // then we can clear the old list
    if(newList.size() == 0) {
        gTangibleList.deleteAll();
        return;
    }
    
    
    // copy so we can edit
    vector<shared_ptr<ofxDTangibleBase> > newList2 = newList;
    
    // if there was no old list
    // we can just fill it with the new
    // ones
    if(oldList.size() == 0) {
        for(int i = newList2.size()-1; i >= 0; i--) {
            // instead of creating a new tangible
            // we can use the ones from newList
            // as long as we remove it from newList
            // else it will be deleted in
            // the tangible finder on the next tangible detection
            gTangibleList.addTangible(newList2[i]);
            newList2.erase(remove(newList2.begin(), newList2.end(), newList2[i]));
            
        }
        return;
    }
    
    
    // make a copy of the old list
    // so we can throw stuff out
    // remember the original is static
    vector<shared_ptr<ofxDTangibleBase> > oldList2 = oldList;
    
    while (true) {
        
        if (oldList2.size() < 1 || newList2.size() < 1) {
            break;
        }
        
        vector<shared_ptr<ofxDTangibleBase> > theClosest2 = closest2(oldList2, newList2);
        
        shared_ptr<ofxDTangibleBase> a = theClosest2[0];
        shared_ptr<ofxDTangibleBase> b = theClosest2[1];
        
        float d = distSquared(a.get(), b.get());
        
        if (d < maxMoveDistSquared) {

            // set the values from b to a           
            // we do this manual instead of a method so we have more control

            a->area = b->area;
            a->length = b->length;
            a->boundingRect = b->boundingRect;
            a->centroid = b->centroid;
            a->hole = b->hole;
            a->pts = b->pts;
            a->nPts = b->nPts;
            
            // in this case we swap the threshold image
            // so a gets the image from b and b gets the image from a
            // we set b so the delete process goes well
            ofxCvGrayscaleImage *tmp = a->thresholdImage;
            a->thresholdImage = b->thresholdImage;
            b->thresholdImage = tmp;
            
            a->thresholdImageOffset = b->thresholdImageOffset;
            a->thresholdImage->setROI(a->thresholdImageOffset.x, a->thresholdImageOffset.y, a->boundingRect.width, a->boundingRect.height);
            
            a->setAngle(b->getAngle());
            a->detectColor = b->detectColor;
            
            a->ptsAngleInfoFirst = b->ptsAngleInfoFirst;
            a->ptsAngleInfoSecond = b->ptsAngleInfoSecond;
            
            // remove it from both lists
            oldList2.erase(remove(oldList2.begin(), oldList2.end(), a));
            newList2.erase(remove(newList2.begin(), newList2.end(), b));
            // now we can delete b
            //delete b;
            
        }
        else {
            // if d was the closest distance possible between elements
            // and the test with maxMoveDistSquared failed
            // then we know that we can quit looking for tangibles
            // that are still on the correct position
            break;
        }
        
    }
    
    // everything that is now in oldList2 is not found again
    // and can be removed
    for (int i = 0; i < oldList2.size(); i++) {
        gTangibleList.deleteTangible(oldList2[i]);
    }
    
    // everything that is left in newList
    // can be added as new instead of a replacement
    for (int i = newList2.size()-1; i >= 0 ; i--) {
        gTangibleList.addTangible(newList2[i]);
        newList2.erase(remove(newList2.begin(), newList2.end(), newList2[i]));
    }
    
    //printf("Time taken: %.5fms\n", ((double)(clock() - tStart)/CLOCKS_PER_SEC)*1000);
    
}



