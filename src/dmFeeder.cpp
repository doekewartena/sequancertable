//
//  feederDisplay.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 11/5/13.
//
//

#include "dmFeeder.h"





//--------------------------------------------------------------

// TODO remove update from draw/control modes?
//
void DMFeeder::update(){
    
    
}


//--------------------------------------------------------------


void DMFeeder::draw(){
    
    ofSetColor(255);
    
    int w = feeder->feedWidth()/2;
    int h = feeder->feedHeight()/2;
    
    feeder->colorImg.draw(0, 0, w, h);
    ofDrawBitmapString("color", 20, 25);
    
    feeder->grayImage.draw(w, 0, w, h);
    ofDrawBitmapString("gray", 20+w, 25);
    
    feeder->grayBg.draw(0, h, w, h);
    ofDrawBitmapString("background", 20, 25+h);
    
    feeder->grayDiff.draw(w, h, w, h);
    ofDrawBitmapString("difference", 20+w, 25+h);
    ofDrawBitmapString("threshold: "+ofToString(feeder->getThreshold()), 20+w, 50+h);
    
    
    ofSetColor(255, 0, 0);
    ofDrawBitmapString(ofToString(ofGetFrameRate()), 20, 50);
    
    
}


//--------------------------------------------------------------


void DMFeeder::keyPressed(int key){
    if (key == '-' || key == '_') {
        feeder->decrementThreshold();
    }
    else if (key == '=' || key == '+') {
        feeder->incrementThreshold();
    }
}

//--------------------------------------------------------------
void DMFeeder::keyReleased(int key){
    if (key == 'b' || key == 'B') {
        feeder->setNewBackground();
    }
}

//--------------------------------------------------------------
void DMFeeder::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void DMFeeder::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMFeeder::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMFeeder::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMFeeder::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void DMFeeder::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void DMFeeder::dragEvent(ofDragInfo dragInfo){
    
}
//--------------------------------------------------------------
void DMFeeder::focus() {
    
}
//--------------------------------------------------------------
void DMFeeder::unfocus() {
    
}
//--------------------------------------------------------------
