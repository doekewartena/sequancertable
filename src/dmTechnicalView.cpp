//
//  dmTechnicalView.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 11/10/13.
//
//

#include "dmTechnicalView.h"
#include "ofxDGlobalTangibleList.h"




//--------------------------------------------------------------


void DMTechnicalView::update(){
    
    
}


//--------------------------------------------------------------


void DMTechnicalView::draw(){
    //ofNoFill();
    //ofLine(0, 0, ofGetWidth(), ofGetHeight());
    ofSetColor(255);
    feeder->grayDiff.draw(0, 0);
    
    
    const vector<shared_ptr<ofxDTangibleBase> > tangibles = gTangibleList.getTangibles();
    
    for (int i = 0 ; i < tangibles.size(); i++) {
        ofxDTangibleBase &tang = *tangibles[i].get();

        // let's test a angle detect method here
        // so we can visualise it
        tang.drawTechnical();
        
        ofNoFill();
        ofSetColor(255,0,0);
        ofEllipse(tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.width, tang.boundingRect.height);
        
        
        // ignore all contour points outside this range
        // TODO, get those radius from knobAngleDetector
        float maxRad = 0.8;
        ofEllipse(tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.width*maxRad, tang.boundingRect.height*maxRad);
        float minRad = 0.4;
        ofEllipse(tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.width*minRad, tang.boundingRect.height*minRad);

        
    }

    
    
}


//--------------------------------------------------------------


void DMTechnicalView::keyPressed(int key){
    
    
    
}

//--------------------------------------------------------------
void DMTechnicalView::keyReleased(int key){
    
}

//--------------------------------------------------------------
void DMTechnicalView::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void DMTechnicalView::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMTechnicalView::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMTechnicalView::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMTechnicalView::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void DMTechnicalView::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void DMTechnicalView::dragEvent(ofDragInfo dragInfo){
    
}
//--------------------------------------------------------------
void DMTechnicalView::focus() {
    
}
//--------------------------------------------------------------
void DMTechnicalView::unfocus() {
    
}
//--------------------------------------------------------------