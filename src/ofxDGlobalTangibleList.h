//
//  ofxDGlobalTangibleList.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/29/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxDTangibleBase.h"


// . . . . . . . . . . . . . . . . . . . . . . . . . . .

class ofxDGlobalTangibleList {
    
    private:
    
        // this contains pointers to used tangibles
        vector<shared_ptr<ofxDTangibleBase> > _tangibles;
    
        
    
    
    public:
    
        ~ofxDGlobalTangibleList() {
            deleteAll();
        }

    
        void addTangible(shared_ptr<ofxDTangibleBase> sp);
        void addTangible(ofxDTangibleBase *t);
        void deleteTangible(shared_ptr<ofxDTangibleBase> sp);
        void deleteTangible(ofxDTangibleBase *t);
        void deleteAll();
    
        int numberOfTangibles() const { return _tangibles.size(); }
    
        const vector<shared_ptr<ofxDTangibleBase> > &getTangibles() const { return _tangibles; }
    
        ofxDTangibleBase* findTangibleByClassName(ofxDTangibleBase* start, const char *className);
    
        ofxDTangibleBase* findTangibleWithinRadius(ofxDTangibleBase* center, ofxDTangibleBase* start, float radius);
        ofxDTangibleBase* findTangibleWithinRadiusByClassName(ofxDTangibleBase* center, ofxDTangibleBase* start, float radius, const char *className);
    
        // findTangiblesInRadius
        // findTangiblesInRadius, []classNames
        // when threading comes, check if it's allowed to change etc.
        // etc
    
    
};

extern ofxDGlobalTangibleList gTangibleList;

