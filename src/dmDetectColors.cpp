//
//  dmDetectColors.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 11/15/13.
//
//

#include "dmDetectColors.h"



//--------------------------------------------------------------


void DMDetectColors::update(){
    
    
}


//--------------------------------------------------------------


void DMDetectColors::draw(){
    
    ofBackground(100);
    


    
    
    
    
 
    
    
    // let's draw the colors
    for (int i = 0; i < impl->detectColors.size(); i++) {
        int y = (i+1) * rowHeight();
        
        ofxDDetectColor *dc = impl->detectColors[i];
        
        ofFill();
        ofSetColor(dc->color);
        ofRect(0, y, infoColumnWidth-marginRight, rowHeight()-marginBottom);
        
        if (i == selectedColorIndex) {
            ofNoFill();
            ofSetColor(255);
            ofRect(0, y, infoColumnWidth-marginRight, rowHeight()-marginBottom);
        }
        
        ofSetColor(255);
        y += 20;
        ofDrawBitmapString(dc->name, 20, y);
        y += 15;
        ofDrawBitmapString(ofToString(dc->fuzziness), 20, y);
        
    }
    
    
    
    
    // we need to know the total width we need
    // for that we look at the sum of the width of the images
    vector<ofxCvColorImage *> originalBlobAreasBy32 = tangibleFinder->originalBlobAreasBy32;
    // we need a column to show the colors as well
    
    
    int totalWidthRequired = 0;

    
    for (int i = 0; i < originalBlobAreasBy32.size(); i++) {
        ofxCvColorImage *img = originalBlobAreasBy32[i];
        totalWidthRequired += img->width + marginRight;
    }
    
    float xScale = (float) (ofGetWidth() - infoColumnWidth) / totalWidthRequired;
    if (xScale > 1) xScale = 1;
    
    
    
    // draw the images
    int x = infoColumnWidth;
    
    ofSetColor(255);
    
    
    vector<ofxCvGrayscaleImage *> thresholdAreasBy32 = tangibleFinder->thresholdAreasBy32;
    
    for (int i = 0; i < originalBlobAreasBy32.size(); i++) {
        ofxCvColorImage *img = originalBlobAreasBy32[i];
        ofSetColor(255);
        
        int y = 0;
        
        // can be that we have to scale even more
        // because the image is higher then the rowHeight
        float yScale = 1.0;
        if (img->height > rowHeight()) {
            yScale = rowHeight() / img->height;
        }
        float scale = min(xScale, yScale);
        
        img->draw(x, y, scale*img->width, scale*img->height);
        
        // now draw for each detectcolor what we got out of it
        for (int j = 0; j < impl->detectColors.size(); j++) {
            
            y = (j+1) * rowHeight();
            
            // draw the image that was the result
            // of the color with the fuzziness
            int index = (i*impl->detectColors.size())+j;
            ofxCvGrayscaleImage *greyImg = thresholdAreasBy32[index];
            greyImg->draw(x, y, scale*greyImg->width, scale*greyImg->height);
        }
        
        
        
        
        x += img->width + 10;
    }
    

}


//--------------------------------------------------------------


void DMDetectColors::keyPressed(int key){
    
    // change fuzziness of selected detectColor
    ofxDDetectColor *dc = impl->detectColors[selectedColorIndex];
    if (key == '-' || key == '_') {
        dc->decrementFuzziness();
    }
    else if (key == '=' || key == '+') {
        dc->incrementFuzziness();
    }
    
    
}

//--------------------------------------------------------------
void DMDetectColors::keyReleased(int key){
    
}

//--------------------------------------------------------------
void DMDetectColors::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void DMDetectColors::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMDetectColors::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMDetectColors::mouseReleased(int x, int y, int button){
    
    // let's check if it's on a detectColor, if so we want to select it
    bool didSelectedColorIndex = false;
    
    if (ofGetMouseX() > 0 && ofGetMouseX() < infoColumnWidth) {
        for (int i = 0; i < impl->detectColors.size(); i++) {
            int y = (i+1) * rowHeight();
            if (ofGetMouseY() >= y && ofGetMouseY() < y + rowHeight()) {
                selectedColorIndex = i;
                didSelectedColorIndex = true;
                break;
            }
        }
    }
    // if we did not select anything let's change the color then
    if (!didSelectedColorIndex) {
        
        if (!screenGrabber.isAllocated()) {
            screenGrabber.allocate(4, 4, OF_IMAGE_COLOR);
        }
        screenGrabber.grabScreen(ofGetMouseX()-2, ofGetMouseY()-2, 4, 4);
        // now let's get the average color out of it
        unsigned char *p = screenGrabber.getPixels();
        float r = 0;
        float g = 0;
        float b = 0;

        for (int y = 0; y < screenGrabber.height; y++) {
            for (int x = 0; x < screenGrabber.width; x++) {
                int pixelIndex = y * (screenGrabber.width*3) + x*3;
                
                r += p[pixelIndex];
                g += p[pixelIndex+1];
                b += p[pixelIndex+2];
            }
        }
        
        int nOfPixels = screenGrabber.width * screenGrabber.height;
        
        r /= nOfPixels;
        g /= nOfPixels;
        b /= nOfPixels;
        
        ofxDDetectColor *dc = impl->detectColors[selectedColorIndex];
        dc->color.set(r, g, b);
        
    }
    
    

}

//--------------------------------------------------------------
void DMDetectColors::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void DMDetectColors::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void DMDetectColors::dragEvent(ofDragInfo dragInfo){
    
}
//--------------------------------------------------------------
void DMDetectColors::focus() {
    tangibleFinder->storeOriginalBlobAreas(true);
    tangibleFinder->storeThresholdAreas(true);
}
//--------------------------------------------------------------
void DMDetectColors::unfocus() {
    // save the overhead
    tangibleFinder->storeOriginalBlobAreas(false);
    tangibleFinder->storeThresholdAreas(false);
    
}
//--------------------------------------------------------------
