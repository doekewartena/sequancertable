//
//  imageBufferFeed.h
//  sequancerTable
//
//  Created by Doeke Wartena on 11/5/13.
//
//

#pragma once

#include "ofMain.h"
#include "tangibleFeeder.h"

class ImageBufferFeed : public TangibleFeeder {
    
    int c = 0;
    
public:
    // constructor
    ImageBufferFeed(string backgroundImage);
    // overwrite methods
    bool hasUpdate();
    void updateColorImage();
    
    //
    vector<ofxCvColorImage*> images;
    
    
    
};
