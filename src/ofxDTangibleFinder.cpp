//
//  ofxTangibleFinder.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/24/13.
//
//

#include "ofxDTangibleFinder.h"
#include "ofxDGlobalMethods.h"



// . . . . . . . . . . . . . . . . . . . . . . . . . . .





int ofxDTangibleFinder::findTangibles(ofxCvGrayscaleImage& greyImg, ofxCvColorImage &colorImg, const vector<ofxDDetectColor*> &detectColors) {
    
    if(!checkForSetupCorrect()) {
        return 0;
    }
    
    
    // TODO
    // only store grayBlobAreas and originalBlobAreas
    // when there is asked fot it since it drains performance a lot
    
    // move them to an unused list
    // to prevend recreating images
    for (int i = 0; i < originalBlobAreasBy32.size(); i++) {
        ofxCvColorImage *img = originalBlobAreasBy32[i];
        unusedCvColorImagesBy32.push_back(img);
    }
    originalBlobAreasBy32.clear();
    // move them to an unused list
    // to prevend recreating images
    for (int i = 0; i < thresholdAreasBy32.size(); i++) {
        ofxCvGrayscaleImage *img = thresholdAreasBy32[i];
        unusedCvGrayscaleImagesBy32.push_back(img);
    }
    thresholdAreasBy32.clear();
    
    
    
    
    
    //printf("_tangibles.size(): %lu\n", _tangibles.size());
    // OPTIMISE: later we could reuse instead of recreate
// shared_ptr now so no neeed to call delete
//    for (int i = 0; i < _tangibles.size(); i++) {
//        delete _tangibles[i];
//    }
    _tangibles.clear();
    
    
    // minArea is pixel based so if we had a perfect black and white image
    // minArea would be minDiameter * PI
    // we use a marge
    float minArea = (float)getMinDiameter() * PI * 0.75;
    contourFinder.findContours(greyImg, minArea, 99999999, getMaxTangibles(), false);
    
    for(int i = 0; i < contourFinder.nBlobs; i++) {
        
        ofxCvBlob &blob = contourFinder.blobs.at(i);
        
        // this blob image can contain multiple tangibles
        // however, if the width or height is less then minDiameter we
        // allready know it's invalid and can continue to the next one
        if (blob.boundingRect.width < getMinDiameter() || blob.boundingRect.height < getMinDiameter()) {
            continue;
        }
        
        unsigned char *ptrColorBig = colorImg.getPixels();
        
        // store the original place
        // for monitor purpose

        if (_storeOriginalBlobAreas) {

            ofxCvColorImage *monitorImg = getCvCvColorImageBy32(blob.boundingRect.width, blob.boundingRect.height);
            
            
            unsigned char *ptrMonitor = monitorImg->getPixels();
            
            int maxX = blob.boundingRect.x + monitorImg->width >= colorImg.width ? blob.boundingRect.width : monitorImg->width;
            int maxY = blob.boundingRect.y + monitorImg->height >= colorImg.height ? blob.boundingRect.height : monitorImg->height;
            
            for (int x = 0; x < maxX; x++) {
                for (int y = 0; y < maxY; y++) {
                    int pixelIndex = y * (monitorImg->width*3) + x*3;
                    
                    int pixelIndexOriginal = (y + blob.boundingRect.y) * (colorImg.width*3) + (x + blob.boundingRect.x)*3;
                
                    ptrMonitor[pixelIndex] = ptrColorBig[pixelIndexOriginal]; // r
                    ptrMonitor[pixelIndex+1] = ptrColorBig[pixelIndexOriginal+1]; // g
                    ptrMonitor[pixelIndex+2] = ptrColorBig[pixelIndexOriginal+2]; // b
                    
                }
            }
            
            monitorImg->flagImageChanged();
        
            originalBlobAreasBy32.push_back(monitorImg);
        }
        
        
        // we use multiple contourFinders in steps of 32 pixels
        // we create a image to fit in that contourFinder size
        ofxCvGrayscaleImage *workImg = getCvGrayscaleImageBy32(blob.boundingRect.width, blob.boundingRect.height);
        ofxCvContourFinder *contourFinder32 = getContourFinderBy32(workImg);
        
        // pointer to grey image pixels
        unsigned char *ptrGrey32 = workImg->getPixels();
        int indexColored;
        int indexGrey;
        
        int minX = blob.boundingRect.x;
        int minY = blob.boundingRect.y;
        
        // we test by every color we have
        // since one workImg can contain multiple colors
        // and one color might work better then the other
        for (int j = 0; j < detectColors.size(); j++) {
            ofxDDetectColor *detectColor = detectColors[j];
            
            // stuff for decide if color get's black or white
            float maxOffset = 255.0*detectColor->fuzziness;
            float minR = detectColor->color.r - maxOffset;
            float maxR = detectColor->color.r + maxOffset;
            float minG = detectColor->color.g - maxOffset;
            float maxG = detectColor->color.g + maxOffset;
            float minB = detectColor->color.b - maxOffset;
            float maxB = detectColor->color.b + maxOffset;
            
            float r, g, b;
            
            
            // we color the part black that goes outside the blob area
            // this is to avoid it's random crap before we send it to the
            // contourFinder.
            // We only have to do this once since we don't manipulate
            // the pixels after it
            if (j == 0) {
                //workImg->set(0);
                
                // first right side
                for (int x = blob.boundingRect.width; x < workImg->width; x++) {
                    for (int y = 0; y < blob.boundingRect.height; y++) {
                        indexGrey = y * workImg->width +  x;
                        ptrGrey32[indexGrey] = 0;
                    }
                }
                // now make the bottom black, apart from the part that we allready collored at the right
                for (int x = 0; x < workImg->width; x++) {
                    for (int y = blob.boundingRect.height; y < workImg->height; y++) {
                        indexGrey = y * workImg->width +  x;
                        ptrGrey32[indexGrey] = 0;
                    }
                }
            }
            
            // add the part of the blob in either black or white pixels
            for (int x = blob.boundingRect.x; x < blob.boundingRect.x + blob.boundingRect.width; x++) {
                for (int y = blob.boundingRect.y; y < blob.boundingRect.y + blob.boundingRect.height; y++) {
                    // p is till our pixel pointer to the colored version
                    indexColored = y * (colorImg.width*3) + x*3;
                    
                    r = ptrColorBig[indexColored];
                    g = ptrColorBig[indexColored+1];
                    b = ptrColorBig[indexColored+2];
                    
                    // we want to start in the left top so we have to substract the start
                    // position of the blob
                    indexGrey = (y - minY) * (workImg->width) + ( x - minX);
                    
                    if (r >= minR && r <= maxR && g >= minG && g <= maxG && b >= minB && b <= maxB ) {
                        ptrGrey32[indexGrey] = 255;
                    }
                    else {
                        ptrGrey32[indexGrey] = 0;
                    }
                    
                }
            }
            
            workImg->flagImageChanged();
            
            if (_storeThresholdAreas) {
                ofxCvGrayscaleImage *monitorImg = getCvGrayscaleImageBy32(workImg->width, workImg->height);
                *monitorImg = *workImg;
                monitorImg->flagImageChanged();
                thresholdAreasBy32.push_back(monitorImg);
            }
            
            // scan now for the blobs
            // once again, minArea and maxArea are pixelbased
            // TODO, make 1.25 value adjustabe? (same for 0.75 in minArea)
            // for some reason maxArea has to be quite high
            // look into this later
            float maxArea = (float)getMaxDiameter() * getMaxDiameter(); //PI * 1.5; // allow a little more
            contourFinder32->findContours(*workImg, minArea, maxArea, getMaxTangibles(), false, false);
            
            
            for (int k = 0; k < contourFinder32->blobs.size(); k++) {
                
                ofxCvBlob &colorBlob = contourFinder32->blobs.at(k);
                
                // we have a potential tangible
                // let's do some checks to see if it meet
                // our requirements
                if (!blobHasValidAspectRatio(colorBlob)) continue;
                if (colorBlob.boundingRect.width < getMinDiameter() ||
                    colorBlob.boundingRect.height < getMinDiameter() ||
                    colorBlob.boundingRect.width > getMaxDiameter() ||
                    colorBlob.boundingRect.height > getMaxDiameter() ) {
                    continue;
                }
                // calculate the center aspect deviation
                float centerCentroidDeviationX = abs(colorBlob.centroid.x - colorBlob.boundingRect.getCenter().x) / colorBlob.boundingRect.getCenter().x;
                //printf("centerCentroidDeviationX: %f\n", centerCentroidDeviationX);
                if (centerCentroidDeviationX > getMaxCentroidDeviation()) {
                    continue;
                }
                float centerCentroidDeviationY = abs(colorBlob.centroid.y - colorBlob.boundingRect.getCenter().y) / colorBlob.boundingRect.getCenter().y;
                //printf("centerCentroidDeviationY: %f\n", centerCentroidDeviationY);
                if (centerCentroidDeviationY > getMaxCentroidDeviation()) {
                    continue;
                }
                
                float thresholdImageOffsetX = colorBlob.boundingRect.x;
                float thresholdImageOffsetY = colorBlob.boundingRect.y;
                
                // move the blob to the correct position (we used a different image remember..)
                colorBlob.centroid.x += blob.boundingRect.x;
                colorBlob.centroid.y += blob.boundingRect.y;
                colorBlob.boundingRect.x += blob.boundingRect.x;
                colorBlob.boundingRect.y += blob.boundingRect.y;
                
                for (int l = 0; l < colorBlob.pts.size(); l++) {
                    colorBlob.pts[l].x += blob.boundingRect.x;
                    colorBlob.pts[l].y += blob.boundingRect.y;
                }
                
                // TODO? check if workImg can be made smaller to another pow of 32
                // now it can be a very large image that contains multiple tangibles
                //ofxDTangibleBase *tang = new ofxDTangibleBase(colorBlob, "ofxDTangibleBase", detectColor, workImg);
                //shared_ptr<ofxDTangibleBase> sp(tang);
                
                //
                // Use of undeclared identifier 'make_shared'
                
                shared_ptr<ofxDTangibleBase> sp(new ofxDTangibleBase(colorBlob, "ofxDTangibleBase", detectColor, workImg));
                //shared_ptr<ofxDTangibleBase> sp = make_shared<ofxDTangibleBase>(colorBlob, "ofxDTangibleBase", detectColor, workImg);
                
                //shared_ptr<ofxDTangibleBase> sp = make_shared<ofxDTangibleBase>(ofxDTangibleBase(colorBlob, "ofxDTangibleBase", detectColor, workImg));
                
                ofxDTangibleBase *tang = sp.get();
                
                // it's unlikely that the blob starts at x0y0
                // so we store the offset so we can draw it correct
                tang->thresholdImageOffset.x = thresholdImageOffsetX;
                tang->thresholdImageOffset.y = thresholdImageOffsetY;
                
                _tangibles.push_back(sp);
            }

        }
        
        unusedCvGrayscaleImagesBy32.push_back(workImg);
        

    }

    
    
    //printf("tangibles found: %lu\n", _tangibles.size());
    
    return _tangibles.size();
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . .






ofxCvGrayscaleImage* ofxDTangibleFinder::getCvGrayscaleImageBy32(int origonalWidth, int originalHeight) {
    
    // figure out what size in steps of 32 do we need for this one.
    float ow = origonalWidth;
    float oh = originalHeight;
    float w = ceil(ow/32) * 32;
    float h = ceil(oh/32) * 32;
    
    // grab a image with this size
    // if it does not exist create it
    
    for (int j = 0; j < unusedCvGrayscaleImagesBy32.size(); j++) {
        ofxCvGrayscaleImage *img = unusedCvGrayscaleImagesBy32[j];
        if (img->width == w && img->height == h) {
            unusedCvGrayscaleImagesBy32.erase(remove(unusedCvGrayscaleImagesBy32.begin(), unusedCvGrayscaleImagesBy32.end(), img));
            return img;
        }
    }
    ofxCvGrayscaleImage *img = new ofxCvGrayscaleImage();
    img->allocate(w, h);
    
    return img;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . .

// TODO create a global class where you can pass the type of image as an parameter
ofxCvColorImage* ofxDTangibleFinder::getCvCvColorImageBy32(int origonalWidth, int originalHeight) {
    // figure out what size in steps of 32 do we need for this one.
    float ow = origonalWidth;
    float oh = originalHeight;
    float w = ceil(ow/32) * 32;
    float h = ceil(oh/32) * 32;
    
    // grab a image with this size
    // if it does not exist create it
    
    for (int j = 0; j < unusedCvColorImagesBy32.size(); j++) {
        ofxCvColorImage *img = unusedCvColorImagesBy32[j];
        if (img->width == w && img->height == h) {
            unusedCvColorImagesBy32.erase(remove(unusedCvColorImagesBy32.begin(), unusedCvColorImagesBy32.end(), img));
            return img;
        }
    }
    ofxCvColorImage *img = new ofxCvColorImage();
    img->allocate(w, h);
    
    return img;
}



// . . . . . . . . . . . . . . . . . . . . . . . . . . .


ofxCvContourFinder* ofxDTangibleFinder::getContourFinderBy32(const ofxCvGrayscaleImage* forImage) {
    
    for (int j = 0; j < contourFindersBy32.size(); j++) {
        ofxCvContourFinder *countourFinder = contourFindersBy32[j];
        if (countourFinder->getWidth() == forImage->width && countourFinder->getHeight() == forImage->height) {
            return countourFinder;
        }
    }

    ofxCvContourFinder *workingContourFinder = new ofxCvContourFinder();
    // the size will be set after the first detection
    contourFindersBy32.push_back(workingContourFinder);
    
    return workingContourFinder;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . .


bool ofxDTangibleFinder::blobHasValidAspectRatio(ofxCvBlob &blob) {
    float aspectRatio = min(blob.boundingRect.getWidth(), blob.boundingRect.getHeight()) / max(blob.boundingRect.getWidth(), blob.boundingRect.getHeight());
    
    return 1 - aspectRatio < getMaxAspectDeviation();
}




// . . . . . . . . . . . . . . . . . . . . . . . . . . .



bool ofxDTangibleFinder::checkForSetupCorrect () {
    if (! _setupCorrect) {
        _setupCorrect = true == setupMinDiameter == setupMaxDiameter == setupMaxTangibles;
        
        if(!setupMinDiameter) {
            printNotSetupError("setupMinDiameter");
        }
        if(!setupMaxDiameter) {
            printNotSetupError("setupMaxDiameter");
        }
        if(!setupMaxTangibles) {
            printNotSetupError("setupMaxTangibles");
        }
    }
    return _setupCorrect;
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . .


void ofxDTangibleFinder::printNotSetupError(string what) {
    std::cout << "ERROR: Need to call method" << what << " before using findTangibles" << std::endl;
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . .