//
//  displayModeBase.h
//  sequancerTable
//
//  Created by Doeke Wartena on 11/5/13.
//
//
#pragma once
#include "ofMain.h"
//#include "sequencerApp.h"
#include "STImplementation.h"



class DisplayModeBase {
    
    

    
    public:
    
        STImplementation *impl;
    
        DisplayModeBase(STImplementation *i) {
            impl = i;
        }
    
        virtual void update() = 0;
        virtual void draw() = 0;
        virtual void keyPressed(int key) = 0;
        virtual void keyReleased(int key) = 0;
        virtual void mouseMoved(int x, int y) = 0;
        virtual void mouseDragged(int x, int y, int button) = 0;
        virtual void mousePressed(int x, int y, int button) = 0;
        virtual void mouseReleased(int x, int y, int button) = 0;
        virtual void windowResized(int w, int h) = 0;
        virtual void dragEvent(ofDragInfo dragInfo) = 0;
        virtual void gotMessage(ofMessage msg) = 0;
    
        virtual void focus() = 0;
        virtual void unfocus() = 0;

    
};