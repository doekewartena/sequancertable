//
//  displayModeExample.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 11/6/13.
//
//

#include "displayModeExample.h"


//--------------------------------------------------------------


void DisplayModeExample::update(){
    
    
}


//--------------------------------------------------------------


void DisplayModeExample::draw(){
    ofNoFill();
    ofLine(0, 0, ofGetWidth(), ofGetHeight());
}


//--------------------------------------------------------------


void DisplayModeExample::keyPressed(int key){
    
    
    
}

//--------------------------------------------------------------
void DisplayModeExample::keyReleased(int key){
    
}

//--------------------------------------------------------------
void DisplayModeExample::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void DisplayModeExample::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DisplayModeExample::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DisplayModeExample::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DisplayModeExample::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void DisplayModeExample::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void DisplayModeExample::dragEvent(ofDragInfo dragInfo){
    
}
//--------------------------------------------------------------
void DisplayModeExample::focus() {
    
}
//--------------------------------------------------------------
void DisplayModeExample::unfocus() {
    
}
//--------------------------------------------------------------


