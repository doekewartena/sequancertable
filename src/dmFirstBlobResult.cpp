//
//  displayModeDetectTangibles.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 11/6/13.
//
//

#include "dmFirstBlobResult.h"
#include "ofxDGlobalTangibleList.h"


//--------------------------------------------------------------


void DMFirstBlobResult::update(){
    
    
}


//--------------------------------------------------------------


void DMFirstBlobResult::draw(){
    ofSetColor(255);

    feeder->grayDiff.draw(0, 0);

    for(int i = 0; i < contourFinder->nBlobs; i++) {
        ofxCvBlob &blob = contourFinder->blobs.at(i);
        blob.draw();
    }
    
    
    ofSetColor(255, 0, 0);
    ofDrawBitmapString("all blobs", 20, 50);
    
    ofDrawBitmapString(ofToString(ofGetFrameRate()), 20, 20);
}


//--------------------------------------------------------------


void DMFirstBlobResult::keyPressed(int key){
    if (key == '-' || key == '_') {
        feeder->decrementThreshold();
    }
    else if (key == '=' || key == '+') {
        feeder->incrementThreshold();
    }
}

//--------------------------------------------------------------
void DMFirstBlobResult::keyReleased(int key){
    if (key == 'b' || key == 'B') {
        feeder->setNewBackground();
    }
}

//--------------------------------------------------------------
void DMFirstBlobResult::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void DMFirstBlobResult::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMFirstBlobResult::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMFirstBlobResult::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMFirstBlobResult::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void DMFirstBlobResult::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void DMFirstBlobResult::dragEvent(ofDragInfo dragInfo){
    
}
//--------------------------------------------------------------
void DMFirstBlobResult::focus() {
    
}
//--------------------------------------------------------------
void DMFirstBlobResult::unfocus() {
    
}
//--------------------------------------------------------------
