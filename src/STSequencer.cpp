//
//  STSequencer.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/28/13.
//
//

#include "STSequencer.h"





void STSequencer::draw() {
    
    // TODO maybe make an (virtual) update method?
    //printf("%f\n", getAngle());
    
    
    
    
    // time since last update
    float t =  ((float) clock() - lastUpdate) / CLOCKS_PER_SEC;
    t *= 1000; // ms
    
    // we look at the difference to the drop angle to calculate the speed
    // first we have to make it possitive
    // TODO, move this to ofxDTangible base (make update method)    
    float dif = getAngle() - dropAngle ;
    if (abs(dif) > PI) {
        if (dif > 0) {
            // clockwise
            dif = TWO_PI-dif;
        }
        else {
            dif = -(TWO_PI-abs(dif));
        }
    }
    
    printf("%f\n", dif);
    
    // we allow negative values for now
    // so we can make it stop moving
    rps = ofMap(dif, -PI, PI, -1, 1);
    
    
    
    //printf("%i\n", ofGetFrameNum());
    //printf("time since last: %f\n", t);
    
    ofSetColor(detectColor->color);
    ofFill();
    ofEllipse(boundingRect.getCenter().x, boundingRect.getCenter().y, boundingRect.width, boundingRect.height);
    
    //angleRadar += 0.1;
    angleRadar += TWO_PI * (rps / 1000) * t;
    
    // draw the radar line
    ofNoFill();
    // TODO rotate
    float x2 = boundingRect.getCenter().x + cos(angleRadar) * radius;
    float y2 = boundingRect.getCenter().y + sin(angleRadar) * radius;
    
    ofLine(boundingRect.getCenter().x, boundingRect.getCenter().y, x2, y2);
    
    
    
    lastUpdate = clock();
    
    
}