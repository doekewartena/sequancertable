//
//  ofxDTangibleCleaner.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 11/27/13.
//
//

#include "ofxDTangibleCleaner.h"
#include "ofxDGlobalMethods.h"


// . . . . . . . . . . . . . . . . . . . . . . . . . . .
/*
bool sortByAreaAscending (const ofxCvBlob* a, const ofxCvBlob* b)
{
    return (a->area < b->area);
}



bool sortByAreaDescending (const ofxCvBlob* a, const ofxCvBlob* b)
{
    return (a->area > b->area);
}
 */

bool sortByAreaAscending (const shared_ptr<ofxDTangibleBase> a, const shared_ptr<ofxDTangibleBase> b)
{
    return (a.get()->area < b.get()->area);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . .


bool sortByAreaDescending (const shared_ptr<ofxDTangibleBase> a, const shared_ptr<ofxDTangibleBase> b)
{
    return (a.get()->area > b.get()->area);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . .



bool sortBySimularityToDetectColor (const shared_ptr<ofxDTangibleBase> a, const shared_ptr<ofxDTangibleBase> b)
{
    return colorSimilarity(a.get()->detectColor->color, a.get()->getColor()) > colorSimilarity(b.get()->detectColor->color, b.get()->getColor());
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . .

int ofxDTangibleCleaner::createColorDetectionPoints(int nTargetPoints) {
    
    colorDetectionVecs.clear();
    
    float step = 1 / sqrt(nTargetPoints);
    step *= sqrt(0.7853982);
    
    for (float y = 0; y < 1; y+=step) {
        for (float x = 0; x < 1; x+=step) {
            if (ofDist(x, y, 0.5, 0.5) <= 0.5) {
                colorDetectionVecs.push_back(ofVec2f(x, y));
            }
        }
    }
    
    //    for(int i = 0; i < colorDetectionVecs.size(); i++) {
    //        printf("ellipse(%f, %f, 1, 1);\n", colorDetectionVecs[i].x*100, colorDetectionVecs[i].y*100);
    //    }
    
    //printf("colorDetectionVecs: %lu\n", colorDetectionVecs.size());
    
    // we scale all a bit smaller now
    // so they stay more within the border
    // the reason we do it here is this way
    // we stay closer to out goal of target points
    
    for (int i = 0; i < colorDetectionVecs.size(); i++) {
        ofVec2f &v = colorDetectionVecs[i];
        // TODO should be based on border properties
        v.x = ofMap(v.x, 0, 1, 0.1, 0.9);
        v.y = ofMap(v.y, 0, 1, 0.1, 0.9);
    }
    
    
    return colorDetectionVecs.size();
}


// . . . . . . . . . . . . . . . . . . . . . . . . . . .

void ofxDTangibleCleaner::clearOverlappingTangibles(const vector<shared_ptr<ofxDTangibleBase> > &tangibles, ofxCvColorImage &img) {
    
    _tangibles.clear();
    
    
    _tangibles = tangibles;
    
    // we create a temporay list so we know which objects to remove
    vector<shared_ptr<ofxDTangibleBase> > toRemove;
    
    
    vector<shared_ptr<ofxDTangibleBase> > intersecting;
    
    for (int i = 0; i < _tangibles.size(); i++) {
        shared_ptr<ofxDTangibleBase> a = _tangibles[i];
        
        if(find(toRemove.begin(), toRemove.end(), a) != toRemove.end()) {
            // skip objects that are allready flagged to be removed
            continue;
        }
        
        // check for intersecting with other tangibles
        // we have to do this radius based since the tangible is round
        // so it can be ok if the rectangle bounding intersects
        
        // we store all intersecting in an array so it also works if more then 2
        // tangibles intersect
        intersecting.clear();
        
        
        for (int j = 0; j < _tangibles.size(); j++) {
            shared_ptr<ofxDTangibleBase> b = _tangibles[j];

            if (a == b) continue;
            
            if(find(toRemove.begin(), toRemove.end(), b) != toRemove.end()) {
                // skip objects that are allready flagged to be removed
                continue;
            }
            
            bool intersects = ovalIntersectsOval(a->boundingRect.getCenter().x, a->boundingRect.getCenter().y, a->boundingRect.width, a->boundingRect.height, b->boundingRect.getCenter().x, b->boundingRect.getCenter().y, b->boundingRect.width, b->boundingRect.height);
            
            if (intersects) {
                intersecting.push_back(b);
            }
            
        }
        
        if (intersecting.size() == 0) {
            continue;
        }
        
        
        // we know have a vector of tangibles that intersect with a
        // we now need to find the one that comes the closest to the
        // detect color.
        // The color will be stored so we don't calculate
        // it multiple times for the same object
        // detectColor(*a.get(), img);
        // we also add a to make things easier
        intersecting.push_back(a);
        for (int j = 0; j < intersecting.size(); j++) {
            detectColor(*intersecting[j].get(), img);
        }
        // now check which color comes to the closest to one of the detect colors
        
        sort(intersecting.begin(), intersecting.end(), sortBySimularityToDetectColor);
        
        for (int j = 1; j < intersecting.size(); j++) {
            toRemove.push_back(intersecting[j]);
        }
        
    }
    
    // now remove from _tangibles the objects in toRemove
    
    //printf("toRemove.size() %lu\n", toRemove.size());
    
    for (int i = 0; i < toRemove.size(); i++) {
        _tangibles.erase(remove(_tangibles.begin(), _tangibles.end(), toRemove[i]));
    }
    
    
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . .

// should this method be in global methods?
void ofxDTangibleCleaner::detectColor(ofxDTangibleBase &t, ofxCvColorImage &img) {
    // check if a color is already detected
    if (t.colorDetectFrameNum() == ofGetFrameNum()) {
        return;
    }
    
    ofRectangle &rec = t.boundingRect;
    
    int xImg, yImg;
    int xMask, yMask;
    
    unsigned char *pImg = img.getPixels();
    // we use the thresholdImage as an mask
    unsigned char *pMask = t.thresholdImage->getPixels();
    
    int pixelIndexImg, pixelIndexMask;
    
    int r, g, b, count;
    r = g = b = count = 0;
    
    for (int i = 0; i < colorDetectionVecs.size(); i++) {
        ofVec2f &v = colorDetectionVecs[i];
        
        xMask = v.x * rec.width + t.thresholdImageOffset.x;
        yMask = v.y * rec.height + t.thresholdImageOffset.y;;
        
        xImg = xMask + rec.x;
        yImg = yMask + rec.y;

        
        pixelIndexMask = yMask * t.thresholdImage->width + xMask;
        
        if(pMask[pixelIndexMask] != 255) {
            //ofEllipse(x, y, 3, 3);
            // where good, now get the colors
            // red
            pixelIndexImg = yImg*(img.width*3)+xImg*3;
            r += pImg[pixelIndexImg];
            g += pImg[pixelIndexImg+1];
            b += pImg[pixelIndexImg+2];
            count++;
        }
    }
    
    // now calculate the average
    if(count != 0) {
        r /= count;
        g /= count;
        b /= count;
    }
    
    
    t.setColor(ofColor(r, g, b));
}



// . . . . . . . . . . . . . . . . . . . . . . . . . . .

