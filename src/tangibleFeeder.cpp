//
//  tangibleFeeder.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/25/13.
//
//

#include "tangibleFeeder.h"


void TangibleFeeder::init() {
    
    if(feedWidth() > 0 && feedHeight() > 0) {
        colorImg.allocate(feedWidth(), feedHeight());        
        colorImg.allocate(feedWidth(), feedHeight());
        grayImage.allocate(feedWidth(), feedHeight());
        grayBg.allocate(feedWidth(), feedHeight());
        grayDiff.allocate(feedWidth(), feedHeight());
    }
    
}

// . . . . . . . . . . . . . . . . . . . . . . . . .


ofxCvGrayscaleImage& TangibleFeeder::getTangibleImage() {
    
    if(ofGetFrameNum() == getTangibleImageFrameNumCall) {
        return grayDiff;
    }
    
    grayImage = colorImg;
    
    if (bSetNewBackground) {
        printf("bSetNewBackground\n");
        grayBg = grayImage;
        bSetNewBackground = false;
    }
    
    grayDiff.absDiff(grayBg, grayImage);
    grayDiff.threshold(getThreshold());
    
    getTangibleImageFrameNumCall = ofGetFrameNum();
    
    return grayDiff;
}


// . . . . . . . . . . . . . . . . . . . . . . . . .


void TangibleFeeder::setNewBackground() {
    bSetNewBackground = true;
}


// . . . . . . . . . . . . . . . . . . . . . . . . .


void TangibleFeeder::setThreshold(int v) {
    if(v >=0 && v <= 255) {
        iThreshold = v;
    }
}

// . . . . . . . . . . . . . . . . . . . . . . . . .

void TangibleFeeder::incrementThreshold() {
    iThreshold++;
    if(iThreshold > 255) iThreshold = 255;
}

// . . . . . . . . . . . . . . . . . . . . . . . . .

void TangibleFeeder::decrementThreshold() {
    iThreshold--;
    if(iThreshold < 0) iThreshold = 0;
}


// . . . . . . . . . . . . . . . . . . . . . . . . .

