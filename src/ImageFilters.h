//
//  ImageFilters.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/27/13.
//
//

#pragma once

#include "ofMain.h"

#ifndef IMAGE_FILTERS
#define IMAGE_FILTERS
void superFastBlurGrayScale(unsigned char *pix, int w, int h, int radius);
void superFastBlur(unsigned char *pix, int w, int h, int radius);
#endif