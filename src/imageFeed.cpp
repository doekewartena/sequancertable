//
//  imageFeed.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/25/13.
//
//
#include "imageFeed.h"


ImageFeed::ImageFeed(string tangibleImage, string backgroundImage) {
    
    ofImage *tmpImage = new ofImage();
    tmpImage->loadImage(backgroundImage);
    
    _feedWidth = tmpImage->getWidth();
    _feedHeight = tmpImage->getHeight();
    
    init();
    
    colorImg.setFromPixels(tmpImage->getPixels(), feedWidth(), feedHeight());
    
    // we call this to set the background
    getTangibleImage();
    
    
    tmpImage->loadImage(tangibleImage);
    colorImg.setFromPixels(tmpImage->getPixels(), feedWidth(), feedHeight());
    

}


// . . . . . . . . . . . . . . . . . . . . . . . . .



bool ImageFeed::hasUpdate() {
    return true;
}



// . . . . . . . . . . . . . . . . . . . . . . . . .


void ImageFeed::updateColorImage() {
    // no need to update sinse we use 1 image atm
}



// . . . . . . . . . . . . . . . . . . . . . . . . .