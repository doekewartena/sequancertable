//
//  STSound.h
//  sequancerTable
//
//  Created by Doeke Wartena on 10/28/13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxDTangibleBase.h"


class STSample : public ofxDTangibleBase {
    
public:
    
    STSample(ofxDTangibleBase& base) : ofxDTangibleBase(base, "STSample") {
        
    }
    
    
};