//
//  dmDetectColors.h
//  sequancerTable
//
//  Created by Doeke Wartena on 11/15/13.
//
//

#pragma once

#include "ofMain.h"
#include "displayModeBase.h"

class DMDetectColors : public DisplayModeBase {
    
    private:
        int selectedColorIndex = 0;
    
    public:
        
        int infoColumnWidth = 100;
        // let's add a litle space inbetween
        int marginRight = 2;
        int marginBottom = 2;
    
        ofImage screenGrabber;
    
        float rowHeight() { return (float) ofGetHeight() / ((float) impl->detectColors.size()+1); }

    
        
        ofxDTangibleFinder *tangibleFinder;
        TangibleFeeder *feeder;
        
        DMDetectColors(STImplementation *i) : DisplayModeBase(i) {
            tangibleFinder = impl->tangibleFinder;
            feeder = impl->feeder;
        }
        
        void update();
        void draw();
        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y);
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);
    
        void focus();
        void unfocus();
    
};
