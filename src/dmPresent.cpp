//
//  dmPresent.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 12/3/13.
//
//

#include "dmPresent.h"
#include "ofxDGlobalTangibleList.h"

//--------------------------------------------------------------


void DMPresent::update(){
    
    
}


//--------------------------------------------------------------


void DMPresent::draw(){
    //ofNoFill();
    //ofLine(0, 0, ofGetWidth(), ofGetHeight());
    //ofSetColor(255);
    //feeder->grayDiff.draw(0, 0);
    
    ofBackground(0);
    
    const vector<shared_ptr<ofxDTangibleBase> > tangibles = gTangibleList.getTangibles();
    
    for (int i = 0 ; i < tangibles.size(); i++) {
        ofxDTangibleBase &tang = *tangibles[i].get();
        
        // let's test a angle detect method here
        // so we can visualise it
        
        
        tang.draw();
        
        /*
        ofNoFill();
        ofSetColor(255,0,0);
        ofEllipse(tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.width, tang.boundingRect.height);
        */
        
        // ignore all contour points outside this range
        // TODO, get those radius from knobAngleDetector
        /*
        float maxRad = 0.8;
        ofEllipse(tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.width*maxRad, tang.boundingRect.height*maxRad);
        float minRad = 0.4;
        ofEllipse(tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.width*minRad, tang.boundingRect.height*minRad);
        
        */
    }
    
    ofSetColor(255, 0, 0);
    ofDrawBitmapString(ofToString(ofGetFrameRate()), 20, 20);

    
}


//--------------------------------------------------------------


void DMPresent::keyPressed(int key){
    
    
    
}

//--------------------------------------------------------------
void DMPresent::keyReleased(int key){
    
}

//--------------------------------------------------------------
void DMPresent::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void DMPresent::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMPresent::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMPresent::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void DMPresent::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void DMPresent::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void DMPresent::dragEvent(ofDragInfo dragInfo){
    
}
//--------------------------------------------------------------
void DMPresent::focus() {
    
}
//--------------------------------------------------------------
void DMPresent::unfocus() {
    
}
//--------------------------------------------------------------