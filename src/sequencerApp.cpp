#include "sequencerApp.h"



#include "STTypes.h"
#include "ofxDGlobalMethods.h"
#include "ofxDGlobalTangibleList.h"

//--------------------------------------------------------------
void SequencerApp::setup(){
    
    
    printf("==================================================================================\n");
    
   
    
    stImplementation = new STImplementation();
    
    
    setMode(DM_FEEDER);
    
    ofSetVerticalSync(true);
    
    ofSetBackgroundAuto(false);
    
    //ofSetFrameRate(1000);

    printf("end of setup\n");
    printf("==================================================================================\n");
    
    
}

//--------------------------------------------------------------


void SequencerApp::update(){
    // we do everything in draw
    // so we can visualise things for debugging
}



//--------------------------------------------------------------



void SequencerApp::draw(){
    
    stImplementation->update();
    
    //printf("begin draw\n");
    if (displayMode != NULL) {
        displayMode->draw();
    }
    //printf("end draw\n");
}

//--------------------------------------------------------------


void SequencerApp::setMode(DisplayModes mode) {
    if (currentDisplayMode == mode) return;
    
    if (displayMode != NULL) {
        displayMode->unfocus();
    }
    
    currentDisplayMode = mode;
    
    if (mode == DM_FEEDER) {
        if (displayModeFeeder == NULL) {
            displayModeFeeder = new DMFeeder(stImplementation);
        }
        displayMode = displayModeFeeder;
    }
    else if(mode == DM_FIRST_BLOB_RESULT) {
        if (displayModeFirstBlobResult == NULL) {
            displayModeFirstBlobResult = new DMFirstBlobResult(stImplementation);
        }
        displayMode = displayModeFirstBlobResult;
    }
    else if(mode == DM_TECHNICAL_VIEW) {
        if (displayModeTechnicalView == NULL) {
            displayModeTechnicalView = new DMTechnicalView(stImplementation);
        }
        displayMode = displayModeTechnicalView;
    }
    else if(mode == DM_DETECT_COLORS) {
        if (displayModeDetectColors == NULL) {
            displayModeDetectColors = new DMDetectColors(stImplementation);
        }
        displayMode = displayModeDetectColors;
    }
    else if(mode == DM_PRESENT) {
        if (displayModePresent == NULL) {
            displayModePresent = new DMPresent(stImplementation);
        }
        displayMode = displayModePresent;
    }
    
    
    displayMode->focus();
    
    
}


//--------------------------------------------------------------



void SequencerApp::keyPressed(int key){
    if (displayMode != NULL) {
        displayMode->keyPressed(key);
    }

}

//--------------------------------------------------------------
void SequencerApp::keyReleased(int key){

    if (key == '1' || key == '!') {
        setMode(DM_FEEDER);
    }
    
    else if (key == '2' || key == '@') {
        setMode(DM_FIRST_BLOB_RESULT);
    }
    else if (key == '3' || key == '#') {
        setMode(DM_DETECT_COLORS);
    }
    else if (key == '4' || key == '$') {
        setMode(DM_TECHNICAL_VIEW);
    }
    else if (key == '5' || key == '%') {
        setMode(DM_PRESENT);
    }
    else if (displayMode != NULL) {
        displayMode->keyReleased(key);
    }
}


//--------------------------------------------------------------





//--------------------------------------------------------------
void SequencerApp::mouseMoved(int x, int y) {
    if (displayMode != NULL) {
        displayMode->mouseMoved(x, y);
    }
}

//--------------------------------------------------------------
void SequencerApp::mouseDragged(int x, int y, int button) {
    if (displayMode != NULL) {
        displayMode->mouseDragged(x, y, button);
    }
}

//--------------------------------------------------------------
void SequencerApp::mousePressed(int x, int y, int button) {
    if (displayMode != NULL) {
        displayMode->mousePressed(x, y, button);
    }
}

//--------------------------------------------------------------
void SequencerApp::mouseReleased(int x, int y, int button) {
    if (displayMode != NULL) {
        displayMode->mouseReleased(x, y, button);
    }
}

//--------------------------------------------------------------
void SequencerApp::windowResized(int w, int h) {
    if (displayMode != NULL) {
        displayMode->windowResized(w, h);
    }
}

//--------------------------------------------------------------
void SequencerApp::gotMessage(ofMessage msg) {
    if (displayMode != NULL) {
        displayMode->gotMessage(msg);
    }
}

//--------------------------------------------------------------
void SequencerApp::dragEvent(ofDragInfo dragInfo) {
    if (displayMode != NULL) {
        displayMode->dragEvent(dragInfo);
    }

}