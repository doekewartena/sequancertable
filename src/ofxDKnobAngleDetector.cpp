//
//  knobAngleDetector.cpp
//  sequancerTable
//
//  Created by Doeke Wartena on 10/15/13.
//
//

#include "ofxDKnobAngleDetector.h"
#include "ofxDGlobalMethods.h"

// temp for testing excecution time
//#include <time.h>


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
/*
ofxDKnobAngleDetector::ofxDKnobAngleDetector() {

}
 */


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


void ofxDKnobAngleDetector::detect(const vector<shared_ptr<ofxDTangibleBase> > &tangibles) {
    
    _tangibles.clear();
    
    for (int i = 0; i < tangibles.size(); i++) {
        ofxDTangibleBase *t = tangibles[i].get();
        
        if(detect(*t)) {
            t->setAngle(fAngle);
            _tangibles.push_back(tangibles[i]);
        }
    }
    
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool ofxDKnobAngleDetector::detect(ofxDTangibleBase &tang) {
    
    // we need to get the 2 longest strokes out of it
    // this will be side a and side b
    
    // first try one stroke
    int startIndex = 0;
    int currentIndex = 0;
    //int endIndex = 0;
    
    bool inProgress = false;
    ofPoint lastPoint;
    
    KnobPtsSideInfo first;
    KnobPtsSideInfo second;
    
    bool complete = false;
    
    
    for (int j = 0; j < tang.nPts; j++) {
        ofPoint point = tang.pts[j];
        
        bool withinMax = inOval(point.x, point.y, tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.width*maxRad, tang.boundingRect.height*maxRad);
        
        bool outsideMin = !inOval(point.x, point.y, tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.width*minRad, tang.boundingRect.height*minRad);
        
        if (withinMax && outsideMin) {
            if (!inProgress) {
                inProgress = true;
                startIndex = j;
                currentIndex = j;
                complete = false;
                //lastPoint = point;
            }
            else {
                // this point has to be connected to the last point
                if (!abs(lastPoint.x - point.x > 1)) {
                    currentIndex = j;
                }
                else {
                    complete = true;
                }
            }
        }
        else if (inProgress){
            // we where in progress but the point is not inside our
            // target range anymore so it's the end of the line
            complete = true;
        }
        
        if (complete && inProgress) {
            // let's check if the one is longer the either first or second
            int length = currentIndex - startIndex;
            
            if (length > first.length()) {
                second.startIndex = first.startIndex;
                second.endIndex = first.endIndex;
                first.startIndex = startIndex;
                first.endIndex = currentIndex;
            }
            else if (length > second.length()) {
                second.startIndex = startIndex;
                second.endIndex = currentIndex;
            }
            // reset for next one
            inProgress = false;
        }
        
        
        lastPoint = point;
        
    }
    
    // let's draw
    /*
    if (first.length() > 0) {
        ofSetColor(0, 255, 0);
        for (int j = first.startIndex; j < first.endIndex; j++) {
            ofEllipse(tang.pts[j].x, tang.pts[j].y, 3, 3);
        }
    }
    if (second.length() > 0) {
        ofSetColor(0, 0, 255);
        for (int j = second.startIndex; j < second.endIndex; j++) {
            ofEllipse(tang.pts[j].x, tang.pts[j].y, 3, 3);
        }
    }
     */
    
    // instead of zero we could set a higher minimum number
    // to define it's valid as tangible
    // we could also define a max difference
    // between first and second
    int minPointsRequired = 5;
    if (first.length() > minPointsRequired && second.length() > minPointsRequired) {
        
        tang.ptsAngleInfoFirst = first;
        tang.ptsAngleInfoSecond = second;
        
        ofVec2f averageA;
        for (int j = first.startIndex; j < first.endIndex; j++) {
            averageA.x += tang.pts[j].x;
            averageA.y += tang.pts[j].y;
        }
        ofVec2f averageB;
        for (int j = second.startIndex; j < second.endIndex; j++) {
            averageB.x += tang.pts[j].x;
            averageB.y += tang.pts[j].y;
        }
        
        averageA.x /= first.length();
        averageA.y /= first.length();
        averageB.x /= second.length();
        averageB.y /= second.length();
        
        
        averageA.x -= tang.boundingRect.getCenter().x;
        averageA.y -= tang.boundingRect.getCenter().y;
        averageB.x -= tang.boundingRect.getCenter().x;
        averageB.y -= tang.boundingRect.getCenter().y;
        
        //float averageHeadingA = -1.0 * atan2(-averageA.y, averageA.x);
        //float averageHeadingB = -1.0 * atan2(-averageB.y, averageB.x);
        fAngle = -1.0 * atan2(-(averageA.y+averageB.y)/2, (averageA.x+averageB.x)/2);
        
        
        //ofLine(tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.getCenter().x + cos(averageHeadingA)*150, tang.boundingRect.getCenter().y + sin(averageHeadingA)*150);
        
        //ofLine(tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.getCenter().x + cos(averageHeadingB)*150, tang.boundingRect.getCenter().y + sin(averageHeadingB)*150);
        
        //ofLine(tang.boundingRect.getCenter().x, tang.boundingRect.getCenter().y, tang.boundingRect.getCenter().x + cos(averageHeadingC)*150, tang.boundingRect.getCenter().y + sin(averageHeadingC)*150);
        
        return true;

        
    }

    return false;
    
    
}




// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
